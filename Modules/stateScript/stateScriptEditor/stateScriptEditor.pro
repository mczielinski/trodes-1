#-------------------------------------------------
#
# Project created by QtCreator 2014-09-29T16:03:48
#
#-------------------------------------------------


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stateScriptEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainWindow.cpp\
        ../scripteditor.cpp \
        ../highlighter.cpp \
    customApplication.cpp

HEADERS  += mainWindow.h\
            ../scripteditor.h \
            ../highlighter.h \
    customApplication.h

macx {
ICON        = trodesMacIcon_sseditor.icns
OTHER_FILES += \
    Info.plist
QMAKE_INFO_PLIST += Info.plist
}

win32 {
RC_ICONS += trodesWindowsIcon_sseditor.ico
}
