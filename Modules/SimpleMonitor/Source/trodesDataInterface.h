#ifndef TRODESDATAINTERFACE_H
#define TRODESDATAINTERFACE_H

#include <QObject>
#include "trodesSocket.h"

class TrodesMessageInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesMessageInterface(QString cfgFileName, QString srvAddr, QString srvPort, QObject *parent = 0);
    ~TrodesMessageInterface();

    TrodesModuleNetwork *trodesModuleInterface;
    QString configFileName;
    QString serverAddress;
    QString serverPort;

signals:
    void quitReceived();
    void statusMessage(QString);
    void newTime(quint32);

public slots:
    void setup();
    void quitNow();

public:
    void Run();
    void timeRecevied(quint32);
    //void queryTimestamp();

};


class TrodesDataInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesDataInterface(QObject *parent = 0);
    ~TrodesDataInterface();

    QTcpSocket*     tcpSocket;


signals:
    void newTime(quint32);
    void statusMessage(QString);

public slots:
    void dataReceived(quint8 inputType, QByteArray msg);
    void dataMessageReceived();
    void connectDataClient(DataTypeSpec *da, int currentDataType);
    void tcpError(QAbstractSocket::SocketError err);
    void Run();

public:

};


#endif // TRODESDATAINTERFACE_H
