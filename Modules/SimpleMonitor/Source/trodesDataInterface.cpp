#include "trodesDataInterface.h"

TrodesMessageInterface::TrodesMessageInterface(QString cfgFileName, QString srvAddr, QString srvPort, QObject *parent) :
    QObject(parent)
    , configFileName(cfgFileName)
    , serverAddress(srvAddr)
    , serverPort(srvPort)
{
      if (!configFileName.isEmpty())
          nsParseTrodesConfig(configFileName); // this creates a bunch of global structs
}

TrodesMessageInterface::~TrodesMessageInterface()
{

}

void TrodesMessageInterface::setup()
{
    //------------------------------------------
    /*
    Module connection protocol:
    1) Trodes (or other master GUI) launches module X, or module X is started directly by the user
    2) Module X defines the provided and needed datatypes
    3) Module X creates a client and connects to Trodes (or the master GUI)
    4) When trodes responds with a module ID, a module server is automatically started if the module has any data available
    5) Module X gives Trodes it’s DataTypesAvailable structure, and it’s data server address
    6) Trodes fills its DataAvailable table entries with the given data, and checks that there are no repeats with other modules
    7) Trodes sends the current DataAvailable list to all currently connected modules.
    ...............
    8) Each module starts one client per needed data type and connects to the proper server
    */

    // Create the  moduleNet structure. This is required for all modules
    trodesModuleInterface = new TrodesModuleNetwork();
    // Define the data type that this module provides
    trodesModuleInterface->dataNeeded = TRODESDATATYPE_CONTINUOUS;
    trodesModuleInterface->useQTSocketsForData = false;

    //Find server address of Trodes (or the master GUI)
    bool connectionSuccess;
    if ((!serverAddress.isEmpty()) &&  (!serverPort.isEmpty())) {
        //The address of the server was specified in the command line,
        //so we use that
        connectionSuccess = trodesModuleInterface->trodesClientConnect(serverAddress,serverPort.toUInt(), true);
    } else {
        //Try to find an existing Trodes server or look in config file.  This will eventually be done
        //with Zeroconf.
        connectionSuccess = trodesModuleInterface->trodesClientConnect();
    }

    if (connectionSuccess) {
        connect(trodesModuleInterface->trodesClient,SIGNAL(quitCommandReceived()),this,SLOT(quitNow()));
//        connect(trodesModuleInterface, SIGNAL(updatedDataAvaialble()), this, SLOT(dataAvailableUpdated()));

        TrodesDataInterface *dataInterface = new TrodesDataInterface();
        QThread *dataThread = new QThread();
        dataInterface->moveToThread(dataThread);
        connect(dataInterface,SIGNAL(newTime(quint32)),this,SIGNAL(newTime(quint32)));
        connect(dataInterface,SIGNAL(statusMessage(QString)),this,SIGNAL(statusMessage(QString)));

        connect(dataThread, SIGNAL(started()), dataInterface, SLOT(Run()));
        dataThread->start();
        connect(trodesModuleInterface, SIGNAL(startDataClient(DataTypeSpec*,int)), dataInterface, SLOT(connectDataClient(DataTypeSpec*,int)));
    }

    if (!connectionSuccess) {
        qDebug() << "Unable to connect with Trodes";
        emit quitNow();
    } else {
        qDebug() << "Starting up";
    }

    trodesModuleInterface->sendModuleName("MessageWatcher");
    emit statusMessage("Messaging connection started");

    Run();

}

void TrodesMessageInterface::quitNow()
{
    QMetaObject::invokeMethod(trodesModuleInterface->trodesClient, "sendQuit", Qt::BlockingQueuedConnection);
    trodesModuleInterface->disconnectClient();
    QThread::msleep(250);
    emit quitReceived();
}

void TrodesMessageInterface::Run()
{
    while (1) {
        this->thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        QThread::usleep(100);
    }
}

TrodesDataInterface::TrodesDataInterface(QObject *parent) : QObject(parent)
{
    tcpSocket = NULL;
}

TrodesDataInterface::~TrodesDataInterface()
{
}

void TrodesDataInterface::dataReceived(quint8 inputType, QByteArray msg) {
    qDebug() << "Message received";
}

void TrodesDataInterface::dataMessageReceived() // must execute in tcpSocket's thread!
{
    TrodesDataStream in(tcpSocket);
    in.setVersion(TrodesDataStream::Qt_4_0);
    quint8 inputType;
    int inputSize;
    quint32 timestamp;
    qint16 dataValue;

    while (tcpSocket->bytesAvailable()) {
        //an inputSize of 0 means that we are not in the middle of reading an unfinished message
        if (inputSize == 0) {
            if (tcpSocket->bytesAvailable() < ((int)sizeof(quint32) + (int)sizeof(quint8))) { //the header has not been fully recieved
                //qDebug() << "nothing in mesage";
                return;
            }

            //If the header has been fully recieved, get the data type and the data size
            in >> inputType;
            in >> inputSize;

            //qDebug() << "Module" << moduleConf->myID << "reading Message Type" << inputType << "size" <<  inputSize;
        }

        if (tcpSocket->bytesAvailable() < inputSize) { //Check to make sure the rest of the message has been fully recieved
            return;
        }

        if (inputType == TRODESDATATYPE_CONTINUOUS) {
            if (inputSize != 6)
                qDebug() << "Wierd message size for continuous data " << inputSize;
            in >> timestamp;
            in >> dataValue;
            emit newTime(timestamp);
        }
        //Reset the inputSize variable for the next message
        inputSize = 0;
    }
}

void TrodesDataInterface::connectDataClient(DataTypeSpec *da, int currentDataType)
{
    if (currentDataType == TRODESDATATYPE_CONTINUOUS){
        // create one client for data from nTrode 0
        int nTrodeWanted = 0;
        int nTrodeIdWanted = spikeConf->ntrodes[nTrodeWanted]->nTrodeId;
        // check to see if this server provides this nTrode
        if ((currentDataType == TRODESDATATYPE_CONTINUOUS) &&
                (da->contNTrodeIndexList.indexOf(nTrodeIdWanted) != -1)) {

            qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
//            tcpSocket = new QTcpSocket(this);
//            connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(dataMessageReceived()));
//            connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tcpError(QAbstractSocket::SocketError)));

//            int ntries = 0;
//            tcpSocket->abort(); //reset the socket in case there is still data there
//            while(ntries < 10) {
//                 tcpSocket->connectToHost(da->hostName,da->hostPort); //connect to host
//                // wait for the connection to be established.
//                if (tcpSocket->waitForConnected(30000) == false) {
//                     qDebug("Unable to connect trodes client socket; retrying");
//                     QObject().thread()->usleep(1000000);
//                     ntries++;
//                }
//                else {
//                    break;
//                }
//            }

//            //sendMessage((quint8) TRODESMESSAGE_SETDATATYPE, currentDataType, nTrodeIdWanted);
//            QByteArray out;
//            TrodesDataStream outStream(&out, QIODevice::WriteOnly);
//            outStream.setVersion(TrodesDataStream::Qt_4_0);
//            outStream << (quint8) TRODESMESSAGE_SETDATATYPE;
//            outStream << (quint32) (sizeof(quint8) + sizeof(qint16));
//            outStream << (quint8) currentDataType << (qint16) nTrodeIdWanted;
//            if(tcpSocket->state() == QAbstractSocket::ConnectedState) {
//                tcpSocket->write(out); //Then, write the message
//                tcpSocket->flush(); // this is critical if we want the write to happen immediately
//            }
//            else {
//                qDebug() << "Socket not ready for writing";
//            }

//            qDebug() << "added client for ntrode " << nTrodeIdWanted;

//            //sendMessage((quint8) TRODESMESSAGE_TURNONDATASTREAM);
//            if(tcpSocket->state() == QAbstractSocket::ConnectedState) {
//                        QByteArray temp;
//                TrodesDataStream msgHeader(&temp,QIODevice::ReadWrite); //Each message has a header
//                msgHeader << (quint8) TRODESMESSAGE_TURNONDATASTREAM;
//                msgHeader << (quint32)0; //Message size is 0
//                tcpSocket->write(temp); //Write the header to the socket
//                tcpSocket->flush(); // this is critical if we want the write to happen immediately
//            }
//            else {
//                qDebug() << "Socket not ready for writing";
//            }
//            qDebug() << "Turn on streaming sent!";

            TrodesClient *tc = new TrodesClient();
            tc->setAddress(da->hostName);
            tc->setPort(da->hostPort);
            tc->setNTrodeId(nTrodeIdWanted);
            tc->setNTrodeIndex(0);
            tcpSocket = tc->tcpSocket;
            disconnect(tcpSocket, SIGNAL(readyRead()), tc, SLOT(readMessage()));
            connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(tcpError(QAbstractSocket::SocketError)));
            tc->connectToHost();
            if (tc->isConnected())
                qDebug() << "Data connected succesfully";
            tc->setDataType(currentDataType, nTrodeIdWanted);
            emit statusMessage(QString("Added client for ntrode %1").arg(nTrodeIdWanted));
            tc->turnOnDataStreaming();
        }
    }
}

void TrodesDataInterface::tcpError(QAbstractSocket::SocketError err)
{
    switch (err) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << "The host was not found. Please check the host name and port settings.";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << "The connection was refused by the peer. "
                    "Make sure the server is running, "
                    "and check that the host name and port "
                    "settings are correct.";
        break;
    case QAbstractSocket::SocketTimeoutError :
        break;
    default:
        qDebug() << "TCP error occured: " << tcpSocket->errorString();
    }

}

//void TrodesDataInterface::dataAvailableUpdated()
//{
//    qDebug() << "Length of dataClient is " << trodesModuleInterface->dataClient.length();
//    qDebug() << "Current thread " << QThread::currentThreadId();

//    if (trodesModuleInterface->dataClient.length() > 0) {
//        for (int i = 0; i < trodesModuleInterface->dataClient.length(); i++ ) {
//            qDebug() << "Data client [" <<i<<"] is of type " << trodesModuleInterface->dataClient[i]->getDataType();
//            connect(trodesModuleInterface->dataClient[i], SIGNAL(messageReceived(quint8,QByteArray)), this,
//                    SLOT(dataReceived(quint8,QByteArray)));
//            TrodesClient *cl = trodesModuleInterface->dataClient[i];
//            if ((cl->getNTrodeIndex() == 0) && (cl->isModuleDataStreamingOn() == false)) {
//                disconnect(cl->tcpSocket, SIGNAL(readyRead()), cl, SLOT(readMessage()));
//                tcpSocket = cl->tcpSocket;
//                connect(cl->tcpSocket, SIGNAL(readyRead()), this, SLOT(dataMessageReceived()), Qt::DirectConnection);
//                qDebug() << "Turning on channel 0 streaming";
//                QMetaObject::invokeMethod(cl, "turnOnDataStreaming", Qt::QueuedConnection);

//            }
//        }
//    }
//}

void TrodesDataInterface::Run()
{
    while (1) {

        if (tcpSocket != NULL) {
            if (tcpSocket->waitForReadyRead(0)) {
                //qDebug() << "reading message on data socket";
                dataMessageReceived();
            }
        }

        this->thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        QThread::usleep(100);
    }
}



