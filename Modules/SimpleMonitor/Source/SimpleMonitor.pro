QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleMonitor
TEMPLATE = app

INCLUDEPATH += ../../../Source/src-config
INCLUDEPATH += ../../../Source/src-main

SOURCES += main.cpp\
        mainWindow.cpp \
    ../../../../Trodes/Source/src-main/trodesSocket.cpp \
    ../../../../Trodes/Source/src-config/configuration.cpp \
    trodesDataInterface.cpp


HEADERS  += mainWindow.h \
    ../../../Source/src-main/trodesSocket.h \
    ../../../Source/src-main/trodesSocketDefines.h \
    ../../../Source/src-config/configuration.h \
    trodesDataInterface.h

