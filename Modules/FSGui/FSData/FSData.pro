#-------------------------------------------------
#
# Project created by QtCreator 2014-10-03T18:23:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FSData
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../../Source/src-main/



SOURCES += \
    fsDataMain.cpp \
    fsSockets.cpp \
    fsProcessData.cpp \
    fsRTFilter.cpp

HEADERS += \
    fsDefines.h \
    stimControlGlobalVar.h \
    fsSocketDefines.h \
    ../../../Source/src-main/trodesSocketDefines.h \
    ../fsSharedStimControlDefines.h

