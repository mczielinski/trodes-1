directory = '/opt/data36/daliu/trodes/templeton/NNF/';
rec_filename = '20151208_NNF_s1';
%channel = [1,1;2,1;3,1;8,1;15,1;16,1;12,4];
%channel = [4,1;9,1;10,1;11,1;13,1;14,1;12,4];
channel = [1,1;2,1;3,1;4,1;8,1;9,1;10,1;11,1;13,1;14,1;15,1;16,1;12,4];

tic;
import_trodes = readTrodesFileContinuous(strcat(directory, rec_filename, '.rec'),channel,0);
toc;

cont_data_ref = (import_trodes.channelData(:,1:end-1) - ...
    repmat(import_trodes.channelData(:,end),[1,size(import_trodes.channelData,2)-1]));

%%
for ii = 1:size(channel,1)-1
    ind = channel(ii,:);
    raw.data{ind(1),ind(2)} = cont_data_ref(:,ii);
end
raw.timestamps = import_trodes.timestamps;
raw.samplingRate = import_trodes.samplingRate;
raw.description = 'Indexing: nTrodeID x nTrodeChannelNum';

%% Trodes 400Hz Lowpass
num = [1 2 1];
den = [1 -1.8209867861 0.8313679495];
gain = 1/385.3132689;
decimate_val = 30000/1500;

for ii = 1:size(channel,1)-1
    ind = channel(ii,:);
    raw_filtered = filter(num, den, cont_data_ref(:,ii))*gain;
    lfp.data{ind(1),ind(2)} = downsample(raw_filtered, decimate_val);
end
lfp.timestamps = downsample(import_trodes.timestamps, decimate_val);
lfp.samplingRate = 1500;
raw.description = 'Indexing: nTrodeID x nTrodeChannelNum';
raw.filter.description = '2nd order Bessel Filter 0-400Hz http://www-users.cs.york.ac.uk/~fisher/mkfilter';
raw.filter.num = num;
raw.filter.dem = den;


%%
save_dir = '/mnt/hotswap/trodes_mat/templeton/NNF/';
%save_filename = '20151208_NNF_r1.mat';

save(strcat(save_dir,rec_filename, '.mat'), 'raw', '-v7.3');
save(strcat(save_dir,rec_filename,'_lfp.mat'), 'lfp', '-v7.3');


%%
pos = readCameraModulePositionFile(strcat(directory, rec_filename, '.videoPositionTracking'));
save(strcat(save_dir,rec_filename,'_pos.mat'), 'pos', '-v7.3');
