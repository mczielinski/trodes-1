#include "abstractexporthandler.h"



AbstractExportHandler::AbstractExportHandler(QStringList arguments)
{

    numChannelsInFile = 0;
    packetTimeLocation = -1;
    packetHeaderSize = -1;
    filePacketSize = -1;

    argumentList = arguments;

    _fileNameFound = false;
    helpMenuPrinted = false;
    argumentReadOk = true;
    _argumentsSupported = true;
    _configReadOk = true;

    filterLowPass = -1;
    filterHighPass = -1;
    useRefs = true;
    useSpikeFilters = true;
    maxGapSizeInterpolation = -1; //default number of points to interpolate over (-1 = inf)
    outputSamplingRate = -1; //default of -1 means to use the same as the input
    //recFileName = "";
    externalWorkspaceFile = "";
    outputFileName = "";

    argumentsProcessed = 0;


}

AbstractExportHandler::~AbstractExportHandler()
{

}

bool AbstractExportHandler::fileNameFound() {
    return _fileNameFound;
}

bool AbstractExportHandler::argumentsOK() {
    return argumentReadOk;
}

bool AbstractExportHandler::argumentsSupported() {
    return _argumentsSupported;
}

bool AbstractExportHandler::wasHelpMenuDisplayed() {
    return helpMenuPrinted;
}

bool AbstractExportHandler::fileConfigOK() {
    return _configReadOk;
}

void AbstractExportHandler::printHelpMenu() {
    printf("\n-rec <filename>  -- Recording filename. Required. Muliple -rec <filename> entries can be used to append data in output.\n"
           "-highpass <integer> -- High pass filter value. Overrides settings in file. \n"
           "-lowpass <integer> -- Low pass filter value. Overrides settings in file.\n"
           "-outputrate <integer> -- Define the output sampling rate in the output file(s).\n"
           "-interp <integer> -- Maximum number of dropped packets to interpolate. \n"
           "-userefs <1 or 0> -- Whether or not to subtract digital references.\n"
           "-usespikefilters <1 or 0> -- Whether or not to apply the spike filter settings in the file. \n"
           "-output <basename> -- The base name for the output files. If not specified, the base name of the first .rec file is used. \n"
           "-reconfig <filename> -- Use a different workspace than the one embedded in the recording file. \n\n\n");
}

void AbstractExportHandler::parseArguments() {



    QString filterLowPass_string = "";
    QString filterHighPass_string = "";
    QString maxGapSizeInterpolation_string = "";
    QString ref_string = "";
    QString outputrate_string = "";
    QString spikefilter_string = "";
    QString reconfig_string = "";
    QString outputname_string = "";

    int optionInd = 1;
    while (optionInd < argumentList.length()) {
        if ((argumentList.at(optionInd).compare("-rec",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            recFileNameList.push_back(argumentList.at(optionInd+1));
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-highpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterHighPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-lowpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterLowPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-interp",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            maxGapSizeInterpolation_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-outputrate",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputrate_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-userefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-usespikefilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            spikefilter_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-reconfig",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            reconfig_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-output",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputname_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }else if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            AbstractExportHandler::printHelpMenu();
            helpMenuPrinted = true;
            argumentsProcessed = argumentsProcessed+1;
            return;
        }
        optionInd++;
    }

    //Read the low pass filter argument
    if (!filterLowPass_string.isEmpty()) {
        bool ok1;

        filterLowPass = filterLowPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Low pass filter could not be resolved into an integer.";
            filterLowPass = -1;
            argumentReadOk = false;
            return;
        } else {


            useSpikeFilters = false; // This setting overrides the file's settings
            if (filterHighPass == -1) {
                //Set the high pass filter to 0
                filterHighPass = 0;
            }
        }
    }

    //Read the high pass filter argument
    if (!filterHighPass_string.isEmpty()) {
        bool ok1;

        filterHighPass = filterHighPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "High pass filter could not be resolved into an integer.";
            filterHighPass = -1;
            argumentReadOk = false;
            return;
        } else {
            useSpikeFilters = false; // This setting overrides the file's settings
            if (filterLowPass == -1) {
                //Set the low pass filter to highest supported value
                filterLowPass = 6000;
            }
        }
    }

    if (!outputrate_string.isEmpty()) {
        bool ok1;

        outputSamplingRate = outputrate_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Output rate could not be resolved into an integer (1 or 0).";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        }

        if ((outputSamplingRate < 0) || (outputSamplingRate > 30000))  {
            qDebug() << "Outputrate must be between 0 and 30000";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        }
    }

    if (!ref_string.isEmpty()) {
        bool ok1;

        useRefs = ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Reference mode could not be resolved into an integer (1 or 0).";
            useRefs = true;
            argumentReadOk = false;
            return;
        }
    }

    if (!spikefilter_string.isEmpty()) {
        bool ok1;

        useSpikeFilters = spikefilter_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Spikefilter mode could not be resolved into an integer (1 or 0).";
            useSpikeFilters = true;
            filterHighPass = -1;
            filterLowPass = -1;
            argumentReadOk = false;
            return;
        }
    }

    //Read the interpolation argument
    if (!maxGapSizeInterpolation_string.isEmpty()) {
        bool ok1;

        maxGapSizeInterpolation = maxGapSizeInterpolation_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Max interpolation gap value could not be resolved into an integer.";
            maxGapSizeInterpolation = -1;
            argumentReadOk = false;
            return;
        }
    }

    if (!reconfig_string.isEmpty()) {

        externalWorkspaceFile = reconfig_string;
        QFileInfo fi(externalWorkspaceFile);
        if (!fi.exists()) {
            qDebug() << "File could not be found: " << externalWorkspaceFile;
            argumentReadOk = false;
        }

    }

    if (!outputname_string.isEmpty()) {

        outputFileName = outputname_string;


    }

    if (recFileNameList.isEmpty()) {
        qDebug() << "No filename(s) given.  Must include -rec <filename> in arguments.";
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        recFileName = recFileNameList.at(0);

        QFileInfo fi(recFileName);

        if (fi.exists()) {
            _fileNameFound = true;
            if (readConfig() < 0) {
                _configReadOk = false;
            }
        } else {
            qDebug() << "File could not be found: " << recFileName;
            argumentReadOk = false;
        }

    }
}

void AbstractExportHandler::createFilters() {

    //Default filter creation-- use the filters that are in the config for each nTrode
    //Redefine in inheriting class if different behavior is required
    for (int i=0; i < channelPacketLocations.length(); i++) {
        channelFilters.push_back(new BesselFilter);
        channelFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);

        if (filterHighPass != -1) {
            //The user has given new filters to override the file's settings
            filterOn[i] = true;
            channelFilters.last()->setFilterRange(filterHighPass,filterLowPass);
        } else if (useSpikeFilters) {
            //Use the spike filter settings in the file
            channelFilters.last()->setFilterRange(highPassFilters[i],lowPassFilters[i]);
        } else {
            //Turn off filters
            filterOn[i] = false;
        }
    }
}

bool AbstractExportHandler::openInputFile() {
    filePtr = new QFile;
    filePtr->setFileName(recFileName);

    //open the raw data file
    if (!filePtr->open(QIODevice::ReadOnly)) {
        delete filePtr;
        qDebug() << "Error: could not open file for reading.";
        return false;
    }

    buffer.resize(filePacketSize*2); //we make the input buffer the size of two packets
    dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    dataLastTimePoint.fill(0);
    pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }

    //Read in a packet of data to make sure everything looks good
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        delete filePtr;
        qDebug() << "Error: could not read from file";
        return false;
    }


    //Look in the trodesComments file to see if time was reset.  If so, create a time offset.
    QFileInfo fi(recFileName);
    QString commentsCheckName = fi.absolutePath() + "/"+ fi.baseName() + ".trodesComments";
    QFileInfo commentsFileInfo(commentsCheckName);
    if (commentsFileInfo.exists()) {
        QFile commentsFile;
        commentsFile.setFileName(commentsFileInfo.absoluteFilePath());
        if (commentsFile.open(QIODevice::ReadOnly)) {
            QString line;

            while (!commentsFile.atEnd()) {
                line += commentsFile.readLine();

                int resetKeywordPos = line.indexOf("time reset");
                if (resetKeywordPos > -1) {

                    qDebug() << "Creating offset for file.";
                    startOffsetTime = currentTimeStamp+1;

                    break;

                }
                line = "";
            }
        }

    }

    //Find first time stamp
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);
    currentTimeStamp = *tPtr + startOffsetTime;
    lastTimeStamp = currentTimeStamp-1;

    //Seek back to begining of data
    filePtr->seek(dataStartLocBytes);

    //Initialize info about progress
    progressMarker = filePtr->size()/10;
    currentProgressMarker = 0;
    lastProgressMod = 100000;
    if (progressMarker == 0) {
        progressMarker = 1;
    }

    //Calculate the number of points to skip to achieve the output rate
    if (outputSamplingRate == -1) {
        decimation = 1;
    } else {
        decimation = hardwareConf->sourceSamplingRate/outputSamplingRate;
    }

    return true;

}

void AbstractExportHandler::printProgress() {
    //Print progress (based on location in input file)
    if ((filePtr->pos()%progressMarker) < lastProgressMod) {
        printf("\r%d%%",currentProgressMarker);
        fflush(stdout);
        currentProgressMarker+=10;
    }
    lastProgressMod = (filePtr->pos()%progressMarker);
}

int AbstractExportHandler::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header

            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (file.pos() < 1000000) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
            }
        }
        file.close();
        return filePos;
    }
}

int AbstractExportHandler::readConfig() {
    //Read the config .xml file

    QString configFileName = recFileName;
    int filePos = 0;
    startOffsetTime = 0;



    QFileInfo fI(configFileName);
    QString baseName = fI.baseName();

    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";
    QFileInfo workspaceFile(workspaceCheckName);



    QDomDocument doc("TrodesConf");


    if (!externalWorkspaceFile.isEmpty()) {
        //An external workspace file should be used
        //this is a normal xml-based config file

        QFile file;
        file.setFileName(externalWorkspaceFile);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(externalWorkspaceFile);
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);

    } else if (workspaceFile.exists()) {
        //Check if there is a .trodeconf file with the same name in the same directory.
        //If so, use that.

        qDebug() << "Using the following workspace file: " << workspaceFile.fileName();
        QFile file;
        file.setFileName(workspaceFile.absoluteFilePath());
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(workspaceFile.absoluteFilePath());
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);
    } else if (!configFileName.isEmpty()) {
        //If the other conditions are not met, use the settings embedded in the recording file.
        QFile file;
        bool isRecFile = false;
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(configFileName);
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();

        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if ((externalWorkspaceFile.isEmpty()) && (!doc.setContent(configContent))) {
                    file.close();
                    qDebug("Config header in didn't read properly.");
                    return -1;
                }
            }
        }
        file.close();
    }

    QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        qDebug("Configuration not root node.");
        return -1;
    }
    nTrodeTable = new NTrodeTable();

    // create the global configuration object and recreate the hardwareConf if it exists.
    globalConf = new GlobalConfiguration(NULL);

    //If there is a hardware conf around, we delete the old one after we make a new one.
    HardwareConfiguration* oldHardwareConf;
    if (hardwareConf != NULL) {
        oldHardwareConf = hardwareConf;
        hardwareConf = new HardwareConfiguration(NULL);
        delete oldHardwareConf;
    } else {
        hardwareConf = new HardwareConfiguration(NULL);
    }


    hardwareConf->sourceSamplingRate = 0;
    hardwareConf->headerSize = 0;
    hardwareConf->NCHAN = 0;


    //Load global options.  Note that this is only for backwards compatibility; new files have a GlobalConfiguration section
    QDomNodeList globalOptions = root.elementsByTagName("GlobalOptions");
    if (globalOptions.length() == 1) {
        // load up the globalConf and hardwareConf options from this section
        QDomNode optionsNode = globalOptions.item(0);
        QDomElement optionElements = optionsNode.toElement();

        int tempNCHAN = optionElements.attribute("numChannels", "0").toInt();
        if (tempNCHAN > 0) {
            hardwareConf->NCHAN = tempNCHAN;
            if ((hardwareConf->NCHAN % 32) != 0) {
                qDebug() << "Config file error: numChannels must be a multiple of 32";
                return -6;
            }
            //qDebug() << "Number of channels: " << hardwareConf->NCHAN;
        }



        int tempSampRate = optionElements.attribute("samplingRate", "0").toInt();
        if (tempSampRate > 0) {
            hardwareConf->sourceSamplingRate = tempSampRate;
            //qDebug() << "Sampling rate: " << hardwareConf->sourceSamplingRate << " Hz";
        }

        int tempHeaderSize = optionElements.attribute("headerSize", "0").toInt();
        if (tempHeaderSize > 0) {
            hardwareConf->headerSize = tempHeaderSize;
            //qDebug() << "Header size: " << hardwareConf->headerSize;
            hardwareConf->headerSizeManuallyDefined = true;
        }

        globalConf->filePrefix = optionElements.attribute("filePrefix", "");
        globalConf->filePath = optionElements.attribute("filePath", "");
    }
    else {
        QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
        if (globalConfigList.length() > 1) {
            qDebug() << "Config file error: multiple GlobalConfiguration sections found in configuration file.";
            return -7;
        }
        QDomNode globalnode = globalConfigList.item(0);
        int globalErrorCode = globalConf->loadFromXML(globalnode);
        if (globalErrorCode != 0) {
            qDebug() << "configuration: nsParseTrodesConfig(): Failed to load global configuration err: " << globalErrorCode;
            return globalErrorCode;
        }
    }

    //------------------------------------------------------------------

    QDomNodeList hardwareConfigList = root.elementsByTagName("HardwareConfiguration");
    if (hardwareConfigList.length() > 1) {
        qDebug() << "Config file error: multiple HardwareConfiguration sections found in configuration file.";
        return -5;
    }
    if (hardwareConfigList.length() > 0) {
        QDomNode hardwarenode = hardwareConfigList.item(0);
        int hardwareErrorCode = hardwareConf->loadFromXML(hardwarenode);
        if (hardwareErrorCode != 0) {
            qDebug() << "configuration: nsParseTrodesConfig(): Failed to load hardware err: " << hardwareErrorCode;
            return hardwareErrorCode;
        }

    }
    else {
        qDebug() << "No hardware configuration found.";
    }



    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE HEADER CONFIGURATION
    //QDomNodeList headerList = root.elementsByTagName("HeaderDisplay");
    QDomNodeList headerList = root.elementsByTagName("AuxDisplayConfiguration");
    if (headerList.length() > 1) {
        qDebug() << "Config file error: multiple AuxDisplayConfiguration sections found in configuration file.";
        return -5;
    } else if (headerList.length() == 0) {
        //Backwards compatibility-- section used to be called 'HeaderDisplay'
        headerList = root.elementsByTagName("HeaderDisplay");
        if (headerList.length() > 1) {
            qDebug() << "Config file error: multiple HeaderDisplay sections found in configuration file.";
            return -5;
        }
    }
    headerConf = new headerDisplayConfiguration(NULL);
    if (headerList.length() > 0) {
        QDomNode headernode = headerList.item(0);
        //headerConf = new headerDisplayConfiguration(NULL);
        int headerErrorCode = headerConf->loadFromXML(headernode);
        if (headerErrorCode != 0) {
            return headerErrorCode;
        }
        //qDebug() << headerConf->headerChannels.length() << " header display channels configured.";
    }
    else {
        qDebug() << "No header display configuration found.";
    }



    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE SPIKE CONFIGURATION
    QDomNodeList list = root.elementsByTagName("SpikeConfiguration");
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "Config file error: either no or multiple SpikeConfiguration sections found in configuration file.";
        return -5;
    }
    QDomNode spikenode = list.item(0);
    int autoNtrodeType = 0;
    autoNtrodeType = spikenode.toElement().attribute("autoPopulate", "0").toInt();
    if (autoNtrodeType > 0) {
        qDebug() << "Using auto population of nTrodes";
        int numTrodes = hardwareConf->NCHAN/autoNtrodeType;
        spikeConf = new SpikeConfiguration(NULL, numTrodes);
    } else {
        spikeConf = new SpikeConfiguration(NULL, spikenode.childNodes().length());
    }

    int spikeErrorCode = spikeConf->loadFromXML(spikenode);

    if (spikeErrorCode != 0) {
        return spikeErrorCode;
    }

    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE STREAMING CONFIGURATION
    list = root.elementsByTagName("StreamDisplay");
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "Config file error: either no or multiple streamDisplay sections found in configuration file.";
        return -4;
    }
    QDomNode eegvisnode = list.item(0);
    streamConf = new streamConfiguration();
    int streamErrorCode = streamConf->loadFromXML(eegvisnode);
    if (streamErrorCode != 0) {
        return streamErrorCode;
    }

    dataStartLocBytes = filePos;

    return filePos; //return the position of the file where data begins

}

void AbstractExportHandler::calculateChannelInfo() {
    //bool useAllChannelsPerNtrode = true;

    packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
    packetTimeLocation = 2*hardwareConf->headerSize;

    QList<int> sortedChannelList;
    //Gather all HW channels
    for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
        spikeDetectors.push_back(new ThresholdSpikeDetector(this,n));
        nTrodeInfo nInf;
        nInf.numChannels = spikeConf->ntrodes[n]->hw_chan.length();
        nInf.lfpChannel = spikeConf->ntrodes[n]->moduleDataChan; //This should really be called lfpDataChan...
        if (useRefs) {
            //User wants to use the reference settings in the file
            nInf.nTrodeRefNtrode = spikeConf->ntrodes[n]->refNTrode;
            nInf.nTrodeRefNtrodeChannel = spikeConf->ntrodes[n]->refChan;
            nInf.nTrodeRefOn = spikeConf->ntrodes[n]->refOn;
        } else {
            //User wants to to turn off all referencing
            nInf.nTrodeRefNtrode = -1;
            nInf.nTrodeRefNtrodeChannel = -1;
            nInf.nTrodeRefOn = false;
        }

        if (useSpikeFilters) {
            //User wants to use spike filter settings in file
            nInf.filterOn = spikeConf->ntrodes[n]->filterOn;
            nInf.nTrodeLowPassFilter = spikeConf->ntrodes[n]->highFilter;
            nInf.nTrodeHighPassFilter = spikeConf->ntrodes[n]->lowFilter;

        } else if (filterHighPass != -1 || filterLowPass != -1){
            //User has entered new filter settings
            nInf.filterOn = true;
            nInf.nTrodeLowPassFilter = filterLowPass;
            nInf.nTrodeHighPassFilter = filterHighPass;

        } else {
            //User wants no filters
            nInf.filterOn = false;
            nInf.nTrodeLowPassFilter = -1;
            nInf.nTrodeHighPassFilter = -1;
        }

        nTrodeSettings.push_back(nInf);
        for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
            int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
            sortedChannelList.push_back(tempHWRead);
        }
    }
    //Sort the channels
    std::sort(sortedChannelList.begin(),sortedChannelList.end());

    //Remember how many channels are actually saved in the file
    //(may be different from how many channels came from recording hardware)
    if (globalConf->saveDisplayedChanOnly) {
        numChannelsInFile = sortedChannelList.length();
    } else {
        numChannelsInFile = hardwareConf->NCHAN;
    }

    filePacketSize = packetHeaderSize+(2*numChannelsInFile);

    //Then we find any unsaved channels, and from that determine the new packet locations
    //of each channel

    int lastChannel = -1;
    int unusedChannelsSoFar = 0;
    for (int i=0;i<sortedChannelList.length();i++) {
        if ((sortedChannelList[i]-lastChannel > 1)&&(globalConf->saveDisplayedChanOnly)) {
            unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);
        }
        channelPacketLocations.push_back((2*hardwareConf->headerSize)+4+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
        lastChannel = sortedChannelList[i];
    }


    //Now we go back and gather some more info about each channel...
    for (int i=0;i<sortedChannelList.length();i++) {
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
                if (tempHWRead == sortedChannelList[i]) {

                    //Keep a record of the channel's nTrode ID
                    nTrodeForChannels.push_back(spikeConf->ntrodes[n]->nTrodeId);
                    nTrodeIndForChannels.push_back(n);
                    //Keep a record of the channel within the nTrode
                    channelInNTrode.push_back(c);
                    //Keep a record of whether the reference is on for this channel
                    refOn.push_back(spikeConf->ntrodes[n]->refOn);

                    //Keep a record of the filter settings for each nTrode
                    filterOn.push_back(spikeConf->ntrodes[n]->filterOn);

                    if (spikeConf->ntrodes[n]->filterOn) {
                        lowPassFilters.push_back(spikeConf->ntrodes[n]->highFilter);
                        highPassFilters.push_back(spikeConf->ntrodes[n]->lowFilter);
                    } else {
                        lowPassFilters.push_back(-1);
                        highPassFilters.push_back(-1);
                    }

                    //Find the packet location in the file of the channel's reference
                    int tempRefHWChan = spikeConf->ntrodes[spikeConf->ntrodes[n]->refNTrode]->hw_chan[spikeConf->ntrodes[n]->refChan];
                    int foundRefIndex = -1;
                    for (int refLookup=0; refLookup < sortedChannelList.length(); refLookup++) {
                        if (sortedChannelList[refLookup] == tempRefHWChan) {
                            foundRefIndex = refLookup;
                            refPacketLocations.push_back(channelPacketLocations[foundRefIndex]);
                            break;
                        }
                    }
                    if (foundRefIndex == -1) {
                        //No ref match was found
                        refPacketLocations.push_back(-1);
                    }

                    //qDebug() << QString("nt%1ch%2").arg(spikeConf->ntrodes[n]->nTrodeId).arg(c+1);
                }
            }
        }
    }


}







