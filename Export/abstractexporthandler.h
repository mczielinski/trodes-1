#ifndef ABSTRACTEXPORTHANDLER_H
#define ABSTRACTEXPORTHANDLER_H

#include <QObject>
#include <QtCore>
#include "configuration.h"
#include "iirFilter.h"
#include "spikeDetectorThread.h"

//Global access to the loaded configuration info and the network object
extern GlobalConfiguration *globalConf;
extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;
extern HardwareConfiguration *hardwareConf;


struct nTrodeInfo {
    bool nTrodeRefOn;
    bool filterOn;
    int nTrodeRefNtrode;
    int nTrodeRefNtrodeChannel;
    int nTrodeLowPassFilter;
    int nTrodeHighPassFilter;
    int numChannels;
    int lfpChannel;
};

class AbstractExportHandler: public QObject
{

    Q_OBJECT

public:
    AbstractExportHandler(QStringList arguments);
    bool fileNameFound();
    bool argumentsOK();
    bool argumentsSupported();
    bool fileConfigOK();
    bool wasHelpMenuDisplayed();
    virtual int processData() = 0; //To be defined in inheriting class
    ~AbstractExportHandler();

protected:
    virtual void printHelpMenu();
    virtual void parseArguments();
    virtual void createFilters();

    int readConfig();
    void calculateChannelInfo();
    bool openInputFile();
    void printProgress();
    int findEndOfConfigSection(QString fileName);

    //virtual void printCustomMenu() = 0; //To be defined in inheriting class
    //virtual void parseCustomArguments(int &argumentsProcessed) = 0; //To be defined in inheriting class


    QStringList argumentList;
    QString recFileName;
    QStringList recFileNameList;
    QString externalWorkspaceFile;
    QString outputFileName;

    int filterLowPass;
    int filterHighPass;
    int maxGapSizeInterpolation;
    bool useRefs;
    bool useSpikeFilters;
    int argumentsProcessed;
    bool argumentReadOk;
    bool _argumentsSupported;
    bool _fileNameFound;
    bool _configReadOk;
    bool helpMenuPrinted;
    int dataStartLocBytes;
    int decimation;
    int outputSamplingRate;


    int numChannelsInFile;
    int packetHeaderSize;
    int packetTimeLocation;
    int filePacketSize;
    uint32_t startOffsetTime;


    QList<int> nTrodeForChannels; //contains theuser-specified ID number for the nTrode
    QList<int> nTrodeIndForChannels; //contains the 0-based index of the nTrode;
    QList<int> channelInNTrode;
    QList<int> channelPacketLocations;
    QList<bool> refOn;
    QList<int> refPacketLocations;

    //Buffer, variables for data reading
    //----------------------------------
    QVector<char> buffer;
    char* bufferPtr;
    uint32_t* tPtr;
    uint32_t currentTimeStamp;
    uint32_t lastTimeStamp;
    int16_t* tempDataPtr;
    int16_t tempDataPoint;
    int16_t tempFilteredDataPoint;
    int16_t tempRefPoint;
    QVector<int16_t> dataLastTimePoint;
    int pointsSinceLastLog;
    QList<BesselFilter*> channelFilters;
    QVector<bool> filterOn;
    QVector<int> lowPassFilters;
    QVector<int> highPassFilters;

    //For nTrode-based organization
    QList <ThresholdSpikeDetector *> spikeDetectors;
    QList <nTrodeInfo> nTrodeSettings;

    //Pointers to the neuro files
    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;




    qint64 progressMarker;
    int currentProgressMarker;
    int lastProgressMod;

    //Input file
    QFile* filePtr;

};

#endif // ABSTRACTEXPORTHANDLER_H
