#ifndef RFDISPLAY_H
#define RFDISPLAY_H

#include <QtGui>
#include "configuration.h"
#include <QtWidgets>
#include "dialogs.h"

class SDDisplayPanel;

class SDDisplay : public QWidget
{
    Q_OBJECT

public:
    SDDisplay(QWidget* parent = 0);
    ~SDDisplay();
    bool addPanel();

private:
    QList<SDDisplayPanel*> controlPanels;
    QGridLayout* panelLayout;
    uint32_t panelSlotsTaken; //A bitwise record of all panels
};




class SDDisplayPanel : public QFrame
{
    Q_OBJECT

public:
    SDDisplayPanel(QWidget* parent = 0);
    ~SDDisplayPanel();
    void setRecordIndex(int index);

public slots:
    void updateSDCardStatus(bool cardConnected,int numChan, bool unlocked, bool hasData);

private slots:
    void connectButtonPressed();
    void enableButtonPressed();
    void reconfigureButtonPressed();
    void downloadButtonPressed();

private:
    int recordIndex;

    QLabel* label;
    QLabel* statusIndicator;
    QTextEdit* consoleWindow;
    TrodesButton* connectButton;
    TrodesButton* enableRecordingButton;
    TrodesButton* reconfigureButton;
    TrodesButton* downloadButton;
    QComboBox*    channelNumSelector;

    bool isCardConnected;
    int numChanConfigured;
    bool cardUnlocked;
    bool cardHasData;

signals:
    void connectionRequest();
    void cardEnableRequest();
    void reconfigureRequest(int numChannels);


};

#endif // RFDISPLAY_H
