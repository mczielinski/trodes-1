/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "streamDisplay.h"
#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "globalObjects.h"


/* ========================================================================= */
/* ========================================================================= */
/* StreamDisplayManager
       StreamDisplayManager is the high level container for the stream display.
   It presumes the existence of a global configuration object,
   "streamConf", which specifies everything. The container object
   manages a multicolumn layout, disributed across multiple tabs.

*/
StreamDisplayManager::StreamDisplayManager(QWidget *parent,StreamProcessorManager* managerPtr) :
    QWidget(parent),
    streamManager(managerPtr),
    displayFrozen(false) {

  int totalChannels = 0;
  for (int trodeCount = 0; trodeCount < spikeConf->ntrodes.length(); trodeCount++) {
      for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[trodeCount]->maxDisp.length(); trodeChan++) {
          totalChannels++;
      }
  }

  //QPixmap pixmap("speaker.png");
  //QIcon ButtonIcon(pixmap);

  QFont labelFont;
  //labelFont.setPixelSize(20);
  labelFont.setFamily("Console");

  int prefferedChanPerColumn = 32;
  int prefferedHeaderChanPerColumn = 20;
  //int prefferedColPerPage = eegDispConf->nColumns;
  int prefferedColPerPage = 2;
  int currentPage = 0;
  int currentColumn = 0;
  int currentTrode = 0;
  int currentChan = 0;
  int currentHeaderChan = 0;
  int firstHeaderColumn = -1;
  int firstHeaderPage = -1;
  bool doneWithNeuralChannels = false;

  if (totalChannels == 0) {
      doneWithNeuralChannels = true;
      firstHeaderPage = 0;
  }

  prefferedChanPerColumn = ceil(totalChannels/streamConf->nColumns);


  QList<int> columnPageAssignment; //which page does each column belong to?
  int unAssignedChannels = totalChannels;
  int unAssingedHeaderChannels = headerConf->headerChannels.length();



  //The streaming display is organized into columns.  In the
  //config file, the user sets the preferred column number.  Here,
  //we calculate how many pages (tabs) will need to be displayed
  //to show all the channels.
  while ((unAssignedChannels > 0)||(unAssingedHeaderChannels > 0)) {

      columnsPerPage.push_back(0); //keep track of how many columns are on each page
      isHeaderDisplayPage.push_back(false);
      bool doneWithPage = false;
      while (!doneWithPage) {

          //add new column to the list
          streamDisplayChannels.append(QList<int>());
          nTrodeIDs.append(QList<int>());
          columnPageAssignment.push_back(currentPage);
          int currentChannelInColumn = 0;
          bool doneWithColumn = false;

          while (!doneWithColumn) {

              if (!doneWithNeuralChannels) {


                  //we do not want any nTrodes to spill over from one column to another,
                  //so we make sure that the next nTrode will fit into the column while
                  //not going over the channel limit per column
                  int numChanInCurrentNTrode = spikeConf->ntrodes[currentTrode]->maxDisp.length();
                  if ((currentChannelInColumn+numChanInCurrentNTrode) <= prefferedChanPerColumn) {
                      for (int trodeChan = 0; trodeChan < numChanInCurrentNTrode; trodeChan++) {
                          streamDisplayChannels[currentColumn].append(spikeConf->ntrodes[currentTrode]->hw_chan[trodeChan]);
//                          streamDisplayChannels[currentColumn].append(currentChan);
                          currentChan++;
                          unAssignedChannels--;
                          currentChannelInColumn++;
                      }
                      currentTrode++;
                  }

                  if ((currentChannelInColumn+numChanInCurrentNTrode) > prefferedChanPerColumn) {
                      doneWithColumn = true;
                  }
                  if (currentChan >= totalChannels) {
                      doneWithColumn = true;
                  }
              } else {

                  streamDisplayChannels[currentColumn].append(currentHeaderChan);
                  isHeaderDisplayPage.last() = true;
                  currentHeaderChan++;
                  currentChannelInColumn++;
                  unAssingedHeaderChannels--;
                  if ((currentChannelInColumn) >= prefferedHeaderChanPerColumn) {
                      doneWithColumn = true;
                  }
                  if (unAssingedHeaderChannels <= 0) {
                      doneWithColumn = true;
                  }
              }

          }
          currentColumn++;

          columnsPerPage.last()++; //increment the number of columns on this page

          if (columnsPerPage.last() >= prefferedColPerPage) {
              doneWithPage = true;
          }
          if ((currentChan >= totalChannels)&&(doneWithNeuralChannels == false)) {
              doneWithPage = true;
              doneWithNeuralChannels = true;

              firstHeaderColumn = currentColumn;
              firstHeaderPage = currentPage+1;

          } else if ((doneWithNeuralChannels == true) && (unAssingedHeaderChannels == 0)) {
              doneWithPage = true;

          }

      } //page
      currentPage++;
  }



  //Now that we have assigned which channel go to which columns, we create
  //the widgets.
  currentColumn = 0;
  currentTrode = 0;
  int currentLabelTrode = 0;
  currentChan = 0;
  currentHeaderChan = 0;
  for (int pInd = 0; pInd < firstHeaderPage; pInd++) {

      eegDisplayLayout.push_back(new QGridLayout());
      eegDisplayWidgets.push_back(new QWidget());
      eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);
      int lastNTrodeID = -999;
      for (int col = 0; col < columnsPerPage[pInd]; col++) {
          int columnChan = 0;
          QGridLayout* numberGrid = new QGridLayout();
          //First determine the number of channels labels that should be displayed

          int numNTrodesInColumn = 0;
          while (columnChan < streamDisplayChannels[currentColumn].length()) {
              for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[currentLabelTrode]->maxDisp.length(); trodeChan++) {
                  columnChan++;
              }
              numNTrodesInColumn++;
          }
          columnChan = 0;
          currentLabelTrode = 0;
          while (columnChan < streamDisplayChannels[currentColumn].length()) {
              for (int trodeChan = 0; trodeChan < spikeConf->ntrodes[currentTrode]->maxDisp.length(); trodeChan++) {

                  QLabel *channelLabel = new QLabel(this);
                  int ntrodeId = spikeConf->ntrodes[currentTrode]->nTrodeId;
                  if (ntrodeId != lastNTrodeID) {

                      currentLabelTrode++;
                      lastNTrodeID = ntrodeId;
                      nTrodeIDs[currentColumn].append(ntrodeId);
                  }
                  //channelLabel->setText(QString::number(ntrode));
                  channelLabel->setText(QString::number(ntrodeId));
                  channelLabel->setAlignment(Qt::AlignCenter);
                  channelLabel->setFont(labelFont);

                  //We only display one label per nTrode
                  if (trodeChan > 0) {                     
                      channelLabel->setVisible(false);
                  } else {
                      if (numNTrodesInColumn < 65) {
                        numberGrid->addWidget(channelLabel,columnChan,0);
                      } else if ((currentLabelTrode % 10) == 0) {
                        numberGrid->addWidget(channelLabel,columnChan,0);
                      }
                      //eegDisplayLayout[currentColumn]->addWidget(channelLabel,row,col);
                  }

                  columnChan++;
                  currentChan++;

              } //trode
              currentTrode++;

          }
          eegDisplayLayout[pInd]->addLayout(numberGrid,0,col*2);
          eegDisplayLayout[pInd]->setColumnMinimumWidth(col*2,30);
          glStreamWidgets.append(new StreamWidgetGL(eegDisplayWidgets.last(), streamDisplayChannels[currentColumn],false, streamManager));

          connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(updateAudioHighlightChannel(int)));
          connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(relayChannelClick(int)));
          connect(glStreamWidgets.last(),SIGNAL(newPSTHTrigger(int,bool)),this,SIGNAL(newPSTHTrigger(int,bool)));
          connect(this,SIGNAL(newPSTHTrigger(int,bool)),glStreamWidgets.last(),SLOT(setPSTHTrigger(int,bool)));

          eegDisplayLayout[pInd]->addWidget(glStreamWidgets.last(), 0, 2*col + 1);

          currentColumn++;

      } //column
      eegDisplayLayout[pInd]->setContentsMargins(10,10,10,10);
      //eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);



  } //page (neural channels)


  //now we do the header channels, if configured for display
  for (int pInd = firstHeaderPage; pInd < currentPage; pInd++) {


      eegDisplayLayout.push_back(new QGridLayout());
      eegDisplayWidgets.push_back(new QWidget());

      eegDisplayWidgets[pInd]->setLayout(eegDisplayLayout[pInd]);

      for (int col = 0; col < columnsPerPage[pInd]; col++) {

          int columnChan = 0;
          QGridLayout* numberGrid = new QGridLayout();
          while (columnChan < streamDisplayChannels[currentColumn].length()) {

                  //Label each channel
                  QLabel *channelLabel = new QLabel(this);

                  //int chanID = headerConf->headerChannels[currentHeaderChan].idNumber;
                  //channelLabel->setText(QString::number(chanID));
                  channelLabel->setText(headerConf->headerChannels[currentHeaderChan].idString);
                  channelLabel->setAlignment(Qt::AlignCenter);
                  channelLabel->setFont(labelFont);
                  numberGrid->addWidget(channelLabel,columnChan,0);

                  columnChan++;
                  currentHeaderChan++;


          }

          eegDisplayLayout[pInd]->addLayout(numberGrid,0,col*2);
          eegDisplayLayout[pInd]->setColumnMinimumWidth(col*2,30);
          glStreamWidgets.append(new StreamWidgetGL(eegDisplayWidgets.last(), streamDisplayChannels[currentColumn],true, streamManager));

          glStreamWidgets.last()->headerDisplay = true; //set the header display flag as true

          //connect(glStreamWidgets.last(),SIGNAL(channelClicked(int)),this,SLOT(relayChannelClick(int)));
          connect(glStreamWidgets.last(),SIGNAL(newPSTHTrigger(int,bool)),this,SIGNAL(newPSTHTrigger(int,bool)));
          connect(this,SIGNAL(newPSTHTrigger(int,bool)),glStreamWidgets.last(),SLOT(setPSTHTrigger(int,bool)));
          eegDisplayLayout[pInd]->addWidget(glStreamWidgets.last(), 0, 2*col + 1);

          currentColumn++;

      } //column
      eegDisplayLayout[pInd]->setContentsMargins(10,10,10,10);

  }

  updateAudioHighlightChannel(streamDisplayChannels[0][0]);

  updateTimer = new QTimer(this);
  connect(updateTimer,SIGNAL(timeout()),this,SLOT(updateAllColumns()));
  updateTimer->start(40); // screen refresh
}

StreamDisplayManager::~StreamDisplayManager() {

    updateTimer->stop();

    while (!glStreamWidgets.isEmpty()) {

        delete glStreamWidgets.takeLast();
    }

}

void StreamDisplayManager::updateAllColumns() {

    if (!displayFrozen && !exportMode && updateTimer->isActive()) {
        for (int i=0; i < glStreamWidgets.length(); i++) {
            glStreamWidgets[i]->update();
        }
    }
}


void StreamDisplayManager::relayChannelClick(int hwchannel) {
    //qDebug() << "[Relay] Channel clicked: " << hwchannel;

    emit trodeSelected(streamConf->trodeIndexLookupByHWChan[hwchannel]);
    emit streamChannelClicked(hwchannel);
}

void StreamDisplayManager::updateAudioHighlightChannel(int hwChan) {
    for (int i = 0; i < glStreamWidgets.length(); i++) {
        glStreamWidgets[i]->setHighlightChannel(hwChan); //sets the highlight on the widget to the current audio channel
    }
}

void StreamDisplayManager::freezeDisplay(bool freeze) {
    displayFrozen = freeze;
}




/* ========================================================================= */


/* ========================================================================= */
/* ========================================================================= */
/* StreamWidgetGL
      StreamWidgetGL is the QGLWidget which actually draws the data. It handles
   amplitude scaling and time length changes based on signals received from
   the global streamConf object. In order to allow for asynchronous updates
   of the data, StreamWidgetGL holds another object DisplayProcessor, which lives
   inside a different thread. The data within this object is what is drawn.
*/

StreamWidgetGL::StreamWidgetGL(QWidget *parent, QList<int> channelList,bool isHeaderDisplay, StreamProcessorManager* managerPtr)
    : QGLWidget(QGLFormat(QGL::DoubleBuffer), parent),
      headerDisplay(isHeaderDisplay),
      streamManager(managerPtr) {

  setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  setAutoFillBackground(false);
  TLength = streamConf->tLength;
  FS = hardwareConf->sourceSamplingRate;
  PSTHTriggerHeaderChannel = -1;
  PSTHTriggerState = true;

  dispHWChannels = channelList;
  nChan = channelList.length();
  currentHighlightChannel = -1;

  //The DisplayProcessor thread also feeds data to the trigger threads, so it's a little
  //awkward that it lives inside this object.  However, it is intuitive that each visual streaming channels
  //box has it's own associated processing thread.

  //dataObj = new DisplayProcessor(0, channelList, groupNumber, headerDisplay);
  //connect(sourceControl, SIGNAL(acquisitionStarted()), dataObj, SLOT(runLoop()));
  //connect(sourceControl, SIGNAL(acquisitionStopped()), this, SLOT(stopAcquisition()));
  backgroundColor = streamConf->backgroundColor;
  maxAmplitude.resize(nChan);
  traceColor.resize(nChan);
  for (int i = 0; i < nChan; i++) {
      if (!headerDisplay) {
        int nt = streamConf->trodeIndexLookupByHWChan[channelList[i]];
        int ch = streamConf->trodeChannelLookupByHWChan[channelList[i]];
        maxAmplitude[i] = (spikeConf->ntrodes[nt]->maxDisp[ch]*65536)/AD_CONVERSION_FACTOR;
        traceColor[i] = spikeConf->ntrodes[nt]->color;
      } else {
          maxAmplitude[i] = headerConf->headerChannels[channelList[i]].maxDisp;
          traceColor[i] = headerConf->headerChannels[channelList[i]].color;

      }
  }

  if (!headerDisplay) {
    connect(spikeConf, SIGNAL(updatedMaxDisplay(void)), this, SLOT(updateMaxDisplay(void)));
    connect(spikeConf, SIGNAL(updatedTraceColor(void)), this, SLOT(updateTraceColor(void)));
    connect(spikeConf, SIGNAL(updatedRef(void)), this, SLOT(setChannels(void)));
    connect(spikeConf, SIGNAL(updatedFilter(void)), this, SLOT(setChannels(void)));
  }

  connect(streamConf,SIGNAL(updatedBackgroundColor(QColor)),this,SLOT(updateBackgroundColor()));

  //No need to set the x-axis, so we skip this step
  //connect(streamConf, SIGNAL(updatedTLength(double)), this, SLOT(setTLength(double)));

}

void StreamWidgetGL::setPSTHTrigger(int headerChannel, bool state) {
    PSTHTriggerHeaderChannel = headerChannel;
    PSTHTriggerState = state;
}

void StreamWidgetGL::stopAcquisition()
{
  //dataObj->quitNow = 1;
}

StreamWidgetGL::~StreamWidgetGL()
{




  //dataObj->quit();
  //dataObj->wait();

  //delete dataObj;

}


void StreamWidgetGL::paintEvent(QPaintEvent *event) {

  makeCurrent();
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  QPen pen;
  //We want the lines to be a set number of pixels in width
  //units per pixel = unitrange/window_width
  pen.setWidth((TLength)/this->geometry().width());



  pen.setStyle(Qt::SolidLine);
  pen.setColor(QColor(100,100,150));
  painter.setPen(pen);
  //painter.setPen(Qt::green);
  painter.setViewTransformEnabled(true);
  painter.setViewport(0,0,width(), height());
  //painter.setViewport(0,0,width()*this->windowHandle()->devicePixelRatio(), height()*this->windowHandle()->devicePixelRatio());

  //painter.setPen(Qt::DotLine);

  //float currentTimeLine = dataObj->dataMinMax[0][2*(dataObj->dataIdx)].x;

  float currentTimeLine;

  if (hardwareConf->NCHAN > 0) {
    int chForTimeLine = spikeConf->ntrodes[0]->hw_chan[0];
    currentTimeLine = streamManager->neuralDataMinMax[chForTimeLine][2*(streamManager->streamProcessors[0]->dataIdx)].x;
  } else {
    currentTimeLine = streamManager->auxDataMinMax[0][2*(streamManager->streamProcessors[0]->dataIdx)].x;
  }

  //Background color
  painter.setWindow(0, 0, TLength*FS, height());
  painter.fillRect(0,0,TLength*FS,height(),QBrush(backgroundColor));

  //Show DIO state for the user-selected channel by highlighting the background
  if (!headerDisplay && PSTHTriggerHeaderChannel > -1) {

      bool currentDState = (streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][0].y > 0);
      int currentStateStart = streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][0].x;
      for (int i=0; i < streamManager->auxDataMinMax[PSTHTriggerHeaderChannel].length(); i = i+2) {
        if (currentDState && streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][i].y == 0) {

            painter.fillRect(currentStateStart,0,streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][i].x-currentStateStart,height(),QBrush(QColor(100,100,150)));
            currentDState = false;
        } else if (!currentDState && streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][i].y > 0) {

            currentStateStart = streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][i].x;
            currentDState = true;
        }

      }
      if (currentDState) {
          painter.fillRect(currentStateStart,0,streamManager->auxDataMinMax[PSTHTriggerHeaderChannel][streamManager->auxDataMinMax[PSTHTriggerHeaderChannel].length()-1].x-currentStateStart,height(),QBrush(QColor(100,100,150)));
      }

  }


  for (int c=0; c < nChan; c++) {

    painter.setWindow(0, -(2*c+1)*maxAmplitude[c], TLength*FS, 2*nChan*maxAmplitude[c]);
    if ((c == currentHighlightChannel)&&(!headerDisplay)) {
      painter.fillRect(0,-maxAmplitude[c],TLength*FS,2*maxAmplitude[c],QBrush(QColor(150,100,100)));
    } else if ((dispHWChannels[c] == PSTHTriggerHeaderChannel)&&(headerDisplay)) {
        painter.fillRect(0,-maxAmplitude[c],TLength*FS,2*maxAmplitude[c],QBrush(QColor(100,100,150)));
    } else {
      //painter.fillRect(0,-maxAmplitude[c],TLength*FS,2*maxAmplitude[c],QBrush(backgroundColor));
    }
    //painter.fillRect(0,-maxAmplitude[c],TLength*FS,2*maxAmplitude[c],QBrush(QColor(100,100,100)));
    //painter.drawLine(currentTimeLine,-maxAmplitude[c],currentTimeLine, maxAmplitude[c]);
  }

  //pen.setWidth(100*TLength);
  int backgroundDarkness = backgroundColor.toCmyk().black();
  if (backgroundDarkness > 150) {
      pen.setColor(Qt::white);
  } else {
    pen.setColor(Qt::black);
  }

  //Draw the vertical time marker line
  pen.setStyle(Qt::DotLine);
  painter.setPen(pen);
  painter.setWindow(0, 0, TLength*FS, height());
  painter.drawLine(currentTimeLine,0,currentTimeLine, height());


  painter.beginNativePainting();
  glViewport(0, 0, (GLint)width()*this->windowHandle()->devicePixelRatio(), (GLint)height()*this->windowHandle()->devicePixelRatio());
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, FS*TLength, 0, nChan, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glPointSize(1);
  glLineWidth(1);


  for (int c=0; c < nChan; c++) {


      qglColor(traceColor[c]);
      glLoadIdentity();
      glTranslatef(0, (nChan - c) - 0.5, 0);
      glScalef(1.0, 1.0/(2*maxAmplitude[c]), 1.0);
      glEnableClientState(GL_VERTEX_ARRAY);

      //plot a line between the maximum and minimum values for each x pixel
      //glVertexPointer(2, GL_FLOAT, 0, dataObj->dataMinMax[c].constData());
      if (!headerDisplay) {
        glVertexPointer(2, GL_FLOAT, 0, streamManager->neuralDataMinMax[dispHWChannels[c]].constData());
      } else {
        glVertexPointer(2, GL_FLOAT, 0, streamManager->auxDataMinMax[dispHWChannels[c]].constData());
      }
      glDrawArrays(GL_LINE_STRIP,0,EEG_TIME_POINTS*2);

      glDisableClientState(GL_VERTEX_ARRAY);
  }

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  painter.endNativePainting();

  //painter.restore();
  painter.end();



  //update(); // this yields continuous updating
}

void StreamWidgetGL::initializeGL()
{
  //glEnable(GL_MULTISAMPLE);
}

void StreamWidgetGL::resizeGL(int w, int h)
{


  glViewport(0, 0, (GLint)(w*this->windowHandle()->devicePixelRatio()), (GLint)(h*this->windowHandle()->devicePixelRatio()));
  //glViewport(0, 0, (GLint)w, (GLint)h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, FS*TLength, 0, nChan, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
}

void StreamWidgetGL::mousePressEvent(QMouseEvent* mouseEvent) {

    //if the user clicked on the plotting widget, determine which channel was closest
    //to the mouse to update the audio

    qreal ypos = mouseEvent->localPos().y();
    //qDebug() << ypos << "/" << height();
    int selection = floor((nChan * (ypos/height())));
    int hwChan = dispHWChannels[selection];

    if (mouseEvent->button() == Qt::LeftButton) {
        if ((!headerDisplay)) {
            emit channelClicked(hwChan);
        }
    } else if (mouseEvent->button() == Qt::RightButton) {
        showChannelContextMenu(mouseEvent->pos(), selection);
    }


 }

void StreamWidgetGL::wheelEvent(QWheelEvent *wheelEvent) {
    qreal ypos = wheelEvent->pos().y();
    int selection = floor((nChan * (ypos/height())));
    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    if (!headerDisplay) {
        int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[selection]];
        int trodeChannel = streamConf->trodeChannelLookupByHWChan[dispHWChannels[selection]];
        int currentMaxDisp = spikeConf->ntrodes[trodeNum]->maxDisp[trodeChannel];
        int newMaxDisp = currentMaxDisp+changeAmount;

        if (newMaxDisp >= 50) {
            //spikeConf->setMaxDisp(trodeNum,trodeChannel,newMaxDisp);
            spikeConf->setMaxDisp(trodeNum,newMaxDisp);
        }
    }

}

void StreamWidgetGL::showChannelContextMenu(const QPoint& pos, int channel) {

    QPoint globalPos = this->mapToGlobal(pos);
    QMenu channelMenu;
    //channelMenu.addAction("nTrode settings...");

    if (headerDisplay) {
        //If this display is for the auxilliary channels, then the context menu is
        //different than for the neural channels.  Here, we set up a menu for things like the
        //PSTH trigger channels, etc.

        bool availableForLogging = false;
        if (headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges) {
            availableForLogging = true;
        }

        channelMenu.addAction(QString("Enable for analysis (increases processor overhead)"));
        channelMenu.actions().last()->setCheckable(true);
        if (availableForLogging) {
            channelMenu.actions().last()->setChecked(true);
        } else {
            channelMenu.actions().last()->setChecked(false);
        }
        channelMenu.addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as PSTH trigger");
        channelMenu.actions().last()->setCheckable(true);
        if (!availableForLogging) {
            channelMenu.actions().last()->setEnabled(false);
        } else if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && PSTHTriggerState){
            channelMenu.actions().last()->setChecked(true);
        }
        channelMenu.addAction(QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as PSTH trigger");
        channelMenu.actions().last()->setCheckable(true);
        if (!availableForLogging) {
            channelMenu.actions().last()->setEnabled(false);
        } else if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && !PSTHTriggerState){
            channelMenu.actions().last()->setChecked(true);
        }
        QAction* selectedItem = channelMenu.exec(globalPos);
        if (selectedItem) {
            // something was chosen
            if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " upward edge as PSTH trigger") {
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && PSTHTriggerState){
                    emit newPSTHTrigger(-1,true);
                } else {
                    emit newPSTHTrigger(dispHWChannels[channel],true);
                }
            } else if (selectedItem->text() == QString("Set ") + headerConf->headerChannels[dispHWChannels[channel]].idString + " downward edge as PSTH trigger") {
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel] && !PSTHTriggerState){
                    emit newPSTHTrigger(-1,true);
                } else {
                    emit newPSTHTrigger(dispHWChannels[channel],false);
                }
            } else if (selectedItem->text() == QString("Enable for analysis (increases processor overhead)")) {
                headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges = !headerConf->headerChannels[dispHWChannels[channel]].storeStateChanges;
                if (PSTHTriggerHeaderChannel == dispHWChannels[channel]) {
                    emit newPSTHTrigger(-1,true);
                }
            }

        } else {
            // nothing was chosen
        }

    } else {
        channelMenu.addAction("Change nTrode color...");
        channelMenu.addAction("Change background color...");
        QAction* selectedItem = channelMenu.exec(globalPos);
        if (selectedItem) {
            // something was chosen, do stuff
            if (selectedItem->text() == "nTrode settings...") {

            } else if (selectedItem->text() == "Change nTrode color...") {
                showNtrodeColorSelector(channel);
            }  else if (selectedItem->text() == "Change background color...") {
                showBackgroundColorSelector();
            }

        } else {
            // nothing was chosen
        }
    }
}

void StreamWidgetGL::showNtrodeColorSelector(int channel) {
    int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[channel]];
    QColorDialog *colorSelector = new QColorDialog(this);
    colorSelector->setCurrentColor(spikeConf->ntrodes[trodeNum]->color);
    colorSelector->setProperty("initialColor",spikeConf->ntrodes[trodeNum]->color);
    colorSelector->setProperty("nTrode",trodeNum);
    connect(colorSelector,SIGNAL(colorSelected(QColor)),this,SLOT(nTrodeColorChosen(QColor)));
    connect(colorSelector,SIGNAL(rejected()),this,SLOT(nTrodeColorSelectorCanceled()));
    colorSelector->exec();
}

void StreamWidgetGL::nTrodeColorChosen(QColor c) {
    int nTrode = sender()->property("nTrode").toInt();
    spikeConf->setColor(nTrode,c);
}

void StreamWidgetGL::nTrodeColorSelectorCanceled() {
    int nTrode = sender()->property("nTrode").toInt();
    QVariant v = sender()->property("initialColor");
    QColor initColor = v.value<QColor>();
    spikeConf->setColor(nTrode,initColor);
}

void StreamWidgetGL::showBackgroundColorSelector() {

    QColorDialog *colorSelector = new QColorDialog(this);
    colorSelector->setCurrentColor(streamConf->backgroundColor);
    colorSelector->setProperty("initialColor",streamConf->backgroundColor);
    connect(colorSelector,SIGNAL(colorSelected(QColor)),this,SLOT(backgroundColorChosen(QColor)));
    connect(colorSelector,SIGNAL(rejected()),this,SLOT(backgroundColorSelectorCanceled()));
    colorSelector->exec();
}

void StreamWidgetGL::backgroundColorChosen(QColor c) {
    streamConf->setBackgroundColor(c);
}

void StreamWidgetGL::backgroundColorSelectorCanceled() {

    QVariant v = sender()->property("initialColor");
    QColor initColor = v.value<QColor>();
    streamConf->setBackgroundColor(initColor);

}



void StreamWidgetGL::setHighlightChannel(int hardwareChannel) {
  currentHighlightChannel = dispHWChannels.indexOf(hardwareChannel);
}

void StreamWidgetGL::updateAxes() {
  makeCurrent();
  resizeGL(width(),height());
}

void StreamWidgetGL::updateMaxDisplay(void){
  // protect totalMaxAmplitude until ready to swap it
  for (int i = 0; i < nChan; i++) {

      maxAmplitude[i] = ((spikeConf->ntrodes[streamConf->trodeIndexLookupByHWChan[dispHWChannels[i]]]->maxDisp[streamConf->trodeChannelLookupByHWChan[dispHWChannels[i]]])*65536)/AD_CONVERSION_FACTOR;

  }
}

void StreamWidgetGL::updateTraceColor(void){
  for (int i = 0; i < nChan; i++) {
      traceColor[i] = spikeConf->ntrodes[streamConf->trodeIndexLookupByHWChan[dispHWChannels[i]]]->color;
  }
}

void StreamWidgetGL::updateBackgroundColor() {
    backgroundColor = streamConf->backgroundColor;
}

void StreamWidgetGL::setChannels(void) {
  //dataObj->updateChannelsFlag = true;
  //qDebug() << "Channel info changed";
}

void StreamWidgetGL::setTLength(double T) {
  TLength = T;
  updateAxes();

}

