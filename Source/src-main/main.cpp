/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QWidget>

#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include "mainWindow.h"
#include "configuration.h"
#include "triggerThread.h"
#include "globalObjects.h"

#include "usbdaqThread.h"
#include "sourceController.h"
#include "customApplication.h"

//#define Num_Char_In_Debug_Buffer 1000

//char Debug_Message_Buffer[Num_Char_In_Debug_Buffer];
//uint32_t Location_In_Debug_Buffer = 0;



void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    std::ofstream outfile;
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "%s\n", localMsg.constData());


        outfile.open("trodesDebugLog.txt", std::ios_base::app);
        outfile << localMsg.constData() << "\n";
        outfile.close();

        /*
        for (int i=0; i < localMsg.length(); i++) {
            Debug_Message_Buffer[Location_In_Debug_Buffer] = localMsg.at(i);
            Location_In_Debug_Buffer = (Location_In_Debug_Buffer+1) % Num_Char_In_Debug_Buffer;
        }*/

        break;
    //case QtInfoMsg:
    //    fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
    //    break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
}


int main(int argc, char *argv[])
{


    std::ofstream outfile;
    outfile.open("trodesDebugLog.txt", std::ios_base::trunc);
    outfile.close();
    qInstallMessageHandler(myMessageOutput);


    customApplication app(argc, argv);
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
    app.setStyle(new Style_tweaks);

    //QApplication app(argc, argv);

    qDebug() << "Starting...";

    //MainWindow w;
    //w.show();
    return app.exec();
}
