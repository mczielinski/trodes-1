/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "trodesSocket.h"
#include <QMessageBox>
#include "time.h"

TrodesDataStream::TrodesDataStream() :
    QDataStream()
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QIODevice *d) :
    QDataStream(d)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QByteArray *a, QIODevice::OpenMode mode) :
    QDataStream(a, mode)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(const QByteArray & a) :
    QDataStream(a)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

DataTypeSpec::DataTypeSpec()
{
    dataType = 0;
}

DataTypeSpec::~DataTypeSpec()
{
};

bool DataTypeSpec::dataTypeSelected()
{
    // returns true if any data type is available
    return (bool)dataType;
}

TrodesDataStream& operator<<(TrodesDataStream& dataStream, const DataTypeSpec& dataTypeSpec)
{
    dataStream << dataTypeSpec.moduleID;
    dataStream << dataTypeSpec.hostName;
    dataStream << dataTypeSpec.hostPort;
    dataStream << dataTypeSpec.socketType;
    dataStream << dataTypeSpec.dataType;
    dataStream << dataTypeSpec.contNTrodeIndexList;
    dataStream << dataTypeSpec.spikeNTrodeIndexList;
    dataStream << dataTypeSpec.contNTrodeUDPPortList;
    dataStream << dataTypeSpec.spikeNTrodeUDPPortList;
    dataStream << dataTypeSpec.digitalIOUDPPort;
    dataStream << dataTypeSpec.analogIOUDPPort;
    dataStream << dataTypeSpec.positionUDPPort;



    return dataStream;
}

// Important: this will throw a UserException on error
TrodesDataStream& operator>>(TrodesDataStream& dataStream, DataTypeSpec& dataTypeSpec)
{
    dataStream >> dataTypeSpec.moduleID;
    dataStream >> dataTypeSpec.hostName;
    dataStream >> dataTypeSpec.hostPort;
    dataStream >> dataTypeSpec.socketType;
    dataStream >> dataTypeSpec.dataType;
    dataStream >> dataTypeSpec.contNTrodeIndexList;
    dataStream >> dataTypeSpec.spikeNTrodeIndexList;
    dataStream >> dataTypeSpec.contNTrodeUDPPortList;
    dataStream >> dataTypeSpec.spikeNTrodeUDPPortList;
    dataStream >> dataTypeSpec.digitalIOUDPPort;
    dataStream >> dataTypeSpec.analogIOUDPPort;
    dataStream >> dataTypeSpec.positionUDPPort;

    return dataStream;
}


DigitalIOSpinBox::DigitalIOSpinBox(QWidget *parent, bool input) :
    QSpinBox(parent),
    input(input)
{
    /* set the range */
    setRange(headerConf->minDigitalPort(input), headerConf->maxDigitalPort(input));
    setValue(headerConf->minDigitalPort(input));
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(updateValue(int)));
    currentIndex = 0;
}



DigitalIOSpinBox::~DigitalIOSpinBox()
{
}



void DigitalIOSpinBox::stepBy(int steps)
{
    int i;

    QStringList IDList;
    QList<int> portList;

    IDList = (input == 1) ? headerConf->digInIDList : headerConf->digOutIDList;
    portList = (input == 1) ? headerConf->digInPortList : headerConf->digOutPortList;

    int newIndex = currentIndex + steps;
    // check to see that this is a valid value
    if (newIndex < 0) newIndex = 0;
    if (newIndex > portList.length()) newIndex = portList.length();

    setValue(portList[newIndex]);

}

void DigitalIOSpinBox::updateValue(int newPort) {
    // see if this is a valid port

    QStringList IDList;
    QList<int> portList;

    IDList = (input == 1) ? headerConf->digInIDList : headerConf->digOutIDList;
    portList = (input == 1) ? headerConf->digInPortList : headerConf->digOutPortList;

    if (portList.indexOf(newPort) == -1) {
        setValue(currentIndex);
    }
    else {
        currentIndex = portList.indexOf(newPort);
    }
}






NTrodeSelectDialog::NTrodeSelectDialog(const QString &title, QWidget *parent) :
    QDialog(parent)
{
    // use the spike configuration informaiton to create a dialog with a list box with one line per nTrode
    this->setWindowTitle(title);


    QGridLayout *mainGrid = new QGridLayout(this);
    QLabel *nTrodeLabel = new QLabel("NTrode");
    nTrodeLabel->setAlignment(Qt::AlignCenter);
    mainGrid->addWidget(nTrodeLabel, 0, 0, 1, 1);
    nTrodeSelector = new QListWidget(this);
    mainGrid->addWidget(nTrodeSelector, 1, 0, 3, 1);

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        // add the item for this ntrode and channel.  Note that the channel numbers are displayed as 1-n, not 0 - n-1
        nTrodeSelector->addItem(QString("%1  chan %2").arg(spikeConf->ntrodes[i]->nTrodeId).arg(spikeConf->ntrodes[i]->moduleDataChan + 1));
    }
    nTrodeSelector->setSelectionMode(QAbstractItemView::ExtendedSelection);

    nSelected = new QLabel("nTrodes Selected: 0");
    mainGrid->addWidget(nSelected, 4, 0, 1, 1);
    connect(nTrodeSelector, SIGNAL(itemPressed(QListWidgetItem*)), this, SLOT(updateSelected(QListWidgetItem*)));

    done = new QPushButton("Done", this);
    connect(done, SIGNAL(pressed()), this, SLOT(hide()));
    mainGrid->addWidget(done, 5, 0, 1, 1);
}

NTrodeSelectDialog::~NTrodeSelectDialog()
{
}

void NTrodeSelectDialog::updateSelected(void)
{
    int numSelected = 0;

    // go through all of the items.  This seems to be necessary when more than one item can be selected
    for (int i = 0; i < nTrodeSelector->count(); i++) {
        bool selected = nTrodeSelector->item(i)->isSelected();
        if (selected) {
            numSelected++;
        }
        emit nTrodeSelected((quint16)i, selected);
    }
    nSelected->setText(QString("nTrodes Selected: %1").arg(numSelected));
}

void NTrodeSelectDialog::updateSelected(QListWidgetItem *item)
{
    updateSelected();
}

void NTrodeSelectDialog::updateNTrodeList(void)
{
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        nTrodeSelector->item(i)->setText(QString("%1  chan %2").arg(spikeConf->ntrodes[i]->nTrodeId).arg(spikeConf->ntrodes[i]->moduleDataChan));
    }
}

void NTrodeSelectDialog::updateNTrodeList(int nTrode, int chan)
{
    // this can be called when the ModuleData channel is updated
    spikeConf->ntrodes[nTrode]->moduleDataChan = chan;
    qDebug() << "updating NTrode List";
    updateNTrodeList();
}

int NTrodeSelectDialog::numberSelected()
{
    return nTrodeSelector->selectedItems().length();
}

int NTrodeSelectDialog::loadFromXML(QDomNode &nTrodeSelectNode)
{
    QDomNodeList nList = nTrodeSelectNode.childNodes();

    QDomElement nt;
    int index;
    bool selected;

    for (int i = 0; i < nList.length(); i++) {
        nt = nList.at(i).toElement();
        index = nt.attribute("nTrodeIndex").toInt();
        nTrodeSelector->item(index)->setSelected((bool)nt.attribute("selected").toInt());
    }
    updateSelected();
    return 1;
}

void NTrodeSelectDialog::saveToXML(QDomDocument &doc, QDomElement &rootNode, bool continuousData)
{
    QDomElement nTrodeConf;

    if (continuousData) {
        nTrodeConf = doc.createElement("contNTrodeSelectConfiguration");
    }
    else {
        nTrodeConf = doc.createElement("spikeNTrodeSelectConfiguration");
    }
    rootNode.appendChild(nTrodeConf);

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        QDomElement nt = doc.createElement("nTrodeInfo");
        nt.setAttribute("nTrodeIndex", i);
        nt.setAttribute("selected", nTrodeSelector->item(i)->isSelected());
        nTrodeConf.appendChild(nt);
    }
}

TrodesMessage::TrodesMessage(QObject *parent) :
    QObject(parent)
{
}

TrodesMessage::TrodesMessage(quint8 mType, QByteArray m, QObject *parent) :
    QObject(parent)
{
    messageType = mType;
    message = m;
}

TrodesMessage::~TrodesMessage()
{
};

TrodesSocketMessageHandler::TrodesSocketMessageHandler(QObject *parent) :
    QObject(parent),
    inputSize(0),
    moduleTime(NULL),
    tcpSocket(NULL),
    socketType(TRODESSOCKETTYPE_TCPIP),
    _hasModulename(false),
    _moduleName(""),
    dataTypeSpecific(false),
    decimation(1), // by default no decimation
    nTrodeId(0), // by default, zero (which for ID's, means no nTrode)
    moduleDataStreaming(false)
{

    sourceSampRate = 30000;
    testTimer.start();
}

bool TrodesSocketMessageHandler::isConnected()
{
    return tcpSocket->isOpen();
}

void TrodesSocketMessageHandler::setSocketType(int sType)
{
    socketType = sType;
}

int TrodesSocketMessageHandler::getSocketType(void)
{
    return socketType;
}

void TrodesSocketMessageHandler::setRemoteHost(QHostAddress rhost)
{
    remoteHost = rhost;
}

QHostAddress TrodesSocketMessageHandler::getRemoteHost(void)
{
    return remoteHost;
}

void TrodesSocketMessageHandler::setRemotePort(quint16 port)
{
    remotePort = port;
}

quint16 TrodesSocketMessageHandler::getRemotePort(void)
{
    return remotePort;
}

void TrodesSocketMessageHandler::setModuleTimePtr(const uint32_t *t)
{
    moduleTime = t;
}

bool TrodesSocketMessageHandler::isDedicatedLine()
{
    return dataTypeSpecific;
}

void TrodesSocketMessageHandler::setDedicatedLine()
{
    dataTypeSpecific = true;
}

void TrodesSocketMessageHandler::setDedicatedLine(quint8 dType)
{
    dataTypeSpecific = true;
    dataType = dType;
}

bool TrodesSocketMessageHandler::isModuleDataStreamingOn()
{
    return moduleDataStreaming;
}

void TrodesSocketMessageHandler::resetSocketForNewThread()
{
    //If the TrodesSocketMessageHandler object has been passed to a different thread
    //then writing to the associated tcpSocket creates problems because it was created
    //in a different thread.  Qt has a way of dealing with this.  First, create a new "dummy"
    //socket, then then set it's socketDesciptor to point to the actual socket.
    QTcpSocket *tmpSocket = tcpSocket;

    tcpSocket = new QTcpSocket(); //TODO: delete later
    tcpSocket->setSocketDescriptor(tmpSocket->socketDescriptor());
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    //connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(disconnectHandler()));
}

void TrodesSocketMessageHandler::resetSocketForNewThread(TrodesSocketMessageHandler *origMessageHandler)
{
    //If the TrodesSocketMessageHandler object has been passed to a different thread
    //then writing to the associated tcpSocket creates problems because it was created
    //in a different thread.  Qt has a way of dealing with this.  First, create a new "dummy"
    //socket, then then set it's socketDesciptor to point to the actual socket.

    tcpSocket = new QTcpSocket();
    tcpSocket->setSocketDescriptor(origMessageHandler->tcpSocket->socketDescriptor());
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    QTcpSocket *tmpSocket = new QTcpSocket;
    // copy the essential variables across
    copyMessageHandlerVar(origMessageHandler);

    origMessageHandler->tcpSocket->setSocketDescriptor(tmpSocket->socketDescriptor());
    origMessageHandler->tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);

    origMessageHandler->tcpSocket->close();

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));

    //disconnect(origMessageHandler->tcpSocket, SIGNAL(readyRead()));
}

void TrodesSocketMessageHandler::copyMessageHandlerVar(TrodesSocketMessageHandler *origMH)
{
    dataType = origMH->getDataType();
    dataTypeSpecific = origMH->isDedicatedLine();
}

void TrodesSocketMessageHandler::setSocket(int socketDescriptor)
{
    //The socket already exists, and the descriptor has been passed
    //(For server side)
    tcpSocket = new QTcpSocket(); //TODO: delete later
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);
    tcpSocket->setSocketDescriptor(socketDescriptor);


    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));

    //connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandler(QAbstractSocket::SocketError)),Qt::DirectConnection);
}

void TrodesSocketMessageHandler::setSocket(QTcpSocket *tcpSocketIn)
{
    //Create a new socket (for client side)
    tcpSocket = tcpSocketIn;
    tcpSocket->setSocketOption(QAbstractSocket::LowDelayOption,1);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(disconnectHandler()));
    //tcpSocket->setParent(this); //the socket needs to have a parent in this thread

    //connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandler(QAbstractSocket::SocketError)),Qt::DirectConnection);
}

TrodesSocketMessageHandler::~TrodesSocketMessageHandler()
{
    tcpSocket->deleteLater();
}


void TrodesSocketMessageHandler::readMessage()
{
    //This function is used to parse Trodes messages

    quint8 inputType;
    bool bytesAvailable = true;
//    if (socketType == TRODESSOCKETTYPE_TCPIP) {
//        TrodesDataStream in;
//        in.setDevice(tcpSocket);
//        in.setVersion(TrodesDataStream::Qt_4_0);

//    }

    quint16 port;
    QHostAddress rHost;
    char messageHeader;
    char udpDatagram[1024];
    QByteArray msg;
    char *temp;

    do {
        //an inputSize of 0 means that we are not in the middle of reading an unfinished message
        if (inputSize == 0) {
            if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
               (tcpSocket->bytesAvailable() < ((int)sizeof(quint32) + (int)sizeof(quint8)))) { //the header has not been fully recieved
                //qDebug() << "nothing in mesage";
                return;
            }
            if ((socketType == TRODESSOCKETTYPE_UDP) &&
                (udpSocket->bytesAvailable() < ((int)sizeof(quint32) + (int)sizeof(quint8)))) { //the header has not been fully recieved
                return;
            }
            //If the header has been fully recieved, get the data type and the data size
            if (socketType == TRODESSOCKETTYPE_TCPIP) {
                tcpSocket->read((char *) &inputType,1);
                tcpSocket->read((char *) &inputSize,sizeof(uint32_t));

                port = tcpSocket->localPort();

            }
            else if (socketType == TRODESSOCKETTYPE_UDP) {
                // we need to read in the whole datagram at once to avoid losing data
                qint64 datagramSize = udpSocket->pendingDatagramSize();
                udpSocket->readDatagram(udpDatagram, datagramSize, &rHost, &port);
                // get the remote host and port for sending data out later
                setRemoteHost(rHost);
                setRemotePort(port);

                port = udpSocket->localPort();

                // parse the message
                inputType = (uint8_t) udpDatagram[0];
                memcpy(&inputSize, &udpDatagram[1], sizeof(uint32_t));
                temp = udpDatagram + 5;
                msg.setRawData(temp, inputSize);
            }


            //qDebug() << "Module" << moduleConf->myID << "port" << port << "reading Message Type" << inputType << "size" <<  inputSize;
        }

        if (socketType == TRODESSOCKETTYPE_TCPIP) {
            //We only want to read in the correct size and leave
            //the next message (if one exists).  To do this, we copy inputSize bytes into
            //a char array, then pass the pointer of the char array to
            //a QByteArray (no aditional memory allocation).  The memory will be de-allocated when all copies of
            //the QByteArray are out of scope.
              if (inputSize > 0) {
                temp = new char[inputSize];
                if (socketType == TRODESSOCKETTYPE_TCPIP) {
                    //in.readRawData(temp, inputSize);
                    if (tcpSocket->read(temp, inputSize) != inputSize) {
                        qDebug() << "read error on socket";
                    }

                }
                msg.setRawData(temp, inputSize);
            }
        }
        //Reset the inputSize variable for the next message
        inputSize = 0;

        if (!dataTypeSpecific) {
            //We only parse incoming messages if the messageHandler
            //has not been switched to a dedicated data line. TO DO: Otherwise,
            //we simply store the incoming stream in a buffer.
            switch (inputType) {
            case TRODESMESSAGE_MODULEID: {
                qint8 id;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> id;
                qDebug() << "Module ID " << id << " received.";
                emit moduleIDReceived(id);
            }
            break;

            case TRODESMESSAGE_DATATYPEAVAILABLE: {
                DataTypeSpec *da = new DataTypeSpec();
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> *da;
                //qDebug() << "[Module " << moduleID << "] got DATATYPEAVAILABLE message from module " << da->moduleID;
                //qDebug() << "port" << da->hostPort << "cont nTrodes" << da->contNTrodeIndexList;


                emit dataAvailableReceived(da);;
            }
            break;

            case TRODESMESSAGE_SENDDATATYPEAVAILABLE: {
                qDebug() << "Got SENDDATATYPEAVILABLE message";
                emit sendDataAvailableReceived(this);
            }
            break;

            case TRODESMESSAGE_SETDATATYPE: {
                // This is used for to set the type of data that should be sent
                quint16 userData;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> dataType >> userData;
                //qDebug() << "In module " << this->getModuleID() << ". Setting dataType to" << dataType;
                emit setDataTypeReceived(this, dataType, userData);
            }
            break;

            case TRODESMESSAGE_NTRODEMODULEDATACHAN: {
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                int tmpNTrode, tmpChan;
                msgDecode >> tmpNTrode >> tmpChan;
                // update the spikeConf structure and emit a signal that we've done so
                spikeConf->ntrodes[tmpNTrode]->moduleDataChan = tmpChan;
                qDebug() << "messageHandler got new nTrode and ModuleData chan" << tmpNTrode << tmpChan;

                emit moduleDataChanUpdated(tmpNTrode, tmpChan);
            }
            break;

            case TRODESMESSAGE_MODULEDISCONNECTED: {
                qint8 ID;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> ID;
                // emit a message that will remove this module's DataAvailable information
                qDebug() << "got message that module disconnected, ID " << ID;
                emit moduleDisconnected(ID);
            }
            break;

            case TRODESMESSAGE_EVENT: {
                //The server sent out an event time
                QString stringMessage;
                QString moduleName;
                int32_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t >> stringMessage >> moduleName;

                //If the module name is empty, try to look it up
                if (moduleName.isEmpty() && hasName()) {
                    moduleName = getModuleName();
                }
                emit eventOccurred(t,stringMessage,moduleName);
            }
            break;

            case TRODESMESSAGE_EVENTLIST: {
                //The server sent out a new event list
                QStringList stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                emit eventListReceived(stringMessage);
            }
            break;

            case TRODESMESSAGE_REQUESTEVENTLIST : {
                //The module requested an event list
                emit eventListReqested(getModuleID());
            }
            break;

            case TRODESMESSAGE_EVENTSUBSCRIBE: {
                //A module is requesting updates for a particular event type.
                //That module wants all times when the event occurs in the future
                QString eventName;
                QString moduleName;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> eventName >> moduleName;
                emit eventUpdatesSubscribe(eventName,moduleName, getModuleID());
            }
            break;

            case TRODESMESSAGE_EVENTUNSUBSCRIBE: {
                //A module is unsubscribing for updates for a particular event type.
                QString eventName;
                QString moduleName;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> eventName >> moduleName;
                emit eventUpdatesUnSubscribe(eventName,moduleName, getModuleID());
            }
            break;

            case TRODESMESSAGE_SUBSCRIPTIONLIST: {
                //The server sent the list of event that the module is currently subscribed to
                QStringList list;

                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> list;
                emit eventSubscriptionListReceived(list);
            }
            break;

            case TRODESMESSAGE_REQUESTSUBSCRIPTIONLIST: {
                //A module is asking what events it is currently subscribed to
                emit eventSubscriptionListRequested(getModuleID());
            }
            break;


            case TRODESMESSAGE_EVENTNAMEREQUEST: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                if (hasName()) {
                    emit eventNameAddReqest(stringMessage, getModuleName(), getModuleID());
                } else {
                    emit eventNameAddReqest(stringMessage, "", getModuleID());
                }
            }
            break;

            case TRODESMESSAGE_EVENTREMOVALREQUEST: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                if (hasName()) {
                    emit eventNameRemoveRequest(stringMessage, getModuleName(), getModuleID());
                } else {
                    emit eventNameRemoveRequest(stringMessage, "", getModuleID());
                }
            }
            break;


            case TRODESMESSAGE_OPENFILE: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                emit openFileEventReceived(stringMessage);
            }
            break;

            case TRODESMESSAGE_SOURCECONNECT: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                emit sourceConnectEventRecieved(stringMessage);
            }
            break;

            case TRODESMESSAGE_MODULENAME: {
                QString stringMessage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> stringMessage;
                _moduleName = stringMessage;
                _hasModulename = true;
                emit nameReceived(this, stringMessage);

                qDebug() << "In module " << moduleID << "got moduleName" << _moduleName;
            }
            break;

            case TRODESMESSAGE_INSTANCENUM: {
                int16_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;

                emit instanceReveivedFromServer(t);
            }
            break;

            case TRODESMESSAGE_TURNONDATASTREAM: {
                // this is a general mess to let trodes know that data streaming is about to be enabled.
                // This disables changes in the moduleData channels and filters
                if (socketType == TRODESSOCKETTYPE_UDP)
                    qDebug() << "messagehandler: got turn on datastream message, port", udpSocket->localPort();
                moduleDataStreaming = true;
                emit moduleDataStreamOn(true);
            }
            break;

            case TRODESMESSAGE_TURNOFFDATASTREAM: {
                qDebug() << "messagehandler: got turn off datastream message";
                moduleDataStreaming = false;
                emit moduleDataStreamOn(false);
            }
            break;

            case TRODESMESSAGE_CLOSEFILE: {
                emit closeFileEventReceived();
            }
            break;

            case TRODESMESSAGE_STARTAQUISITION: {
                emit startAquisitionEventReceived();
            }
            break;

            case TRODESMESSAGE_STOPAQUISITION: {
                emit stopAquisitionEventReceived();
            }
            break;

            case TRODESMESSAGE_SETTLECOMMAND: {
                emit settleCommandTriggered();
            }
            break;

            case TRODESMESSAGE_CURRENTTIMEREQUEST: {
                //qDebug() << "Got time request " << QThread::currentThreadId();;
                //Immediately answer with the current time
                if (moduleTime != NULL) {
                    sendCurrentTime(*moduleTime);
                }

                //emit timeRequestReceived(this);
            }
            break;

            case TRODESMESSAGE_CURRENTTIME: {
                uint32_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;
                quint32 outtime = (quint32)t;
                emit currentTimeReceived(outtime);
            }
            break;

            case TRODESMESSAGE_TIMERATEREQUEST: {
                //qDebug() << "Got time rate request " << QThread::currentThreadId();;
                //Immediately answer with the current time rate

                sendTimeRate();
            }
            break;

            case TRODESMESSAGE_TIMERATE: {
                uint32_t t;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> t;
                quint32 outtime = (quint32)t;
                emit timeRateReceived(t);
            }
            break;

            case TRODESMESSAGE_CURRENTSTATEREQUEST: {
                //Used to get the current open file name and save state
                emit currentStateRequested(this);
            }
            break;


            case TRODESMESSAGE_STATESCRIPTCOMMAND: {
                //This is a command for the statescipt controller.  Convert the array to a QString
                QString stringMessage(msg);
                //TrodesDataStream msgDecode(&msg,QIODevice::ReadOnly);
                //msgDecode >> stringMessage;
                //qDebug() << "got stateScript command" << stringMessage;

                emit stateScriptCommandReceived(stringMessage);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE0: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                qDebug() << "Image recieved";
                emit cameraImageRecieved(cameraImage, 0);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE1: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                emit cameraImageRecieved(cameraImage, 1);
            }
            break;

            case TRODESMESSAGE_CAMERAIMAGE2: {
                QImage cameraImage;
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> cameraImage;
                emit cameraImageRecieved(cameraImage, 2);
            }
            break;

            case TRODESMESSAGE_QUIT: {
                qDebug() << "Got quit command";
                emit quitCommandReceived();
            }
            break;

            default: {
                emit messageReceived(inputType, msg);
            }
            break;
            }
        }
        else {
            //Message parsing largely disabled for dedicated line, but these calls allow a connecting client to turn on/off or set parameters for the data stream
            switch (inputType) {
            case TRODESMESSAGE_TURNONDATASTREAM: {
                qDebug() << "data handler: got turn on datastream message, NTrodeId" << nTrodeId;
                moduleDataStreaming = true;
            }
            break;

            case TRODESMESSAGE_TURNOFFDATASTREAM: {
                moduleDataStreaming = false;
                qDebug() << "data handler: got turn off datastream message NTrodeId" << nTrodeId;
            }
            break;

            case TRODESMESSAGE_SETDECIMATION: {
                TrodesDataStream msgDecode(&msg, QIODevice::ReadOnly);
                msgDecode >> decimation;
                //qDebug() << "data handler: got decimation" << decimation;
                break;
            }

            default:
                // add the message to a circular buffer
                //emit messageReceived(inputType, msg);
                qDebug() << "Undecoded message" << inputType << "size" << inputSize;

                break;
            }
        }
        //qDebug() << "in readmessage, bytes available = " << tcpSocket->bytesAvailable();
        bytesAvailable = ((socketType == TRODESSOCKETTYPE_TCPIP) && (tcpSocket->bytesAvailable())) ||
                         ((socketType == TRODESSOCKETTYPE_UDP) && (udpSocket->bytesAvailable()));

     } while (bytesAvailable);
}


//Lower-level function to send data
void TrodesSocketMessageHandler::sendMessage(quint8 dataType, int message)
{
    // put the message variable in a ByteArray
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite);
    msg << message;
    sendMessage(dataType, temp);
}



//Lower-level function to send a message.  This is used for communication with non-qt modules
void TrodesSocketMessageHandler::sendMessage(quint8 messageType, const char *message, quint32 messageSize)
{
    if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
        (tcpSocket->state() == QAbstractSocket::ConnectedState)) {
        tcpSocket->write((char*)&messageType, sizeof(quint8));
        tcpSocket->write((char*)&messageSize, sizeof(uint32_t));
        tcpSocket->write(message, (qint64)messageSize);
        tcpSocket->flush(); // this is critical if we want the write to happen immediately
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        // create a single character array with the data, as udp datagrams must be written all at once
        char data[1024], *dataptr;
        dataptr = data;
        *dataptr = messageType;
        dataptr++;
        memcpy(dataptr, &messageSize, sizeof(uint32_t));
        dataptr += sizeof(uint32_t);
        memcpy(dataptr, message, messageSize);
        udpSocket->writeDatagram(data, sizeof(quint8)+sizeof(uint32_t)+messageSize, getRemoteHost(), getRemotePort());
        //qDebug() << "sent data on port" << udpSocket->localPort();
    }
    else {
        qDebug() << "Socket not ready for writing";
    }
}

void TrodesSocketMessageHandler::sendMessage(quint8 dataType, QByteArray message)
{
    //qDebug() << "TrodesSocketMessageHandler: sending message";
    QByteArray temp;
    TrodesDataStream msg(&temp, QIODevice::ReadWrite); //Each message has a header
    msg << dataType; //The first part of the header is the data type
    //The next part is a 32-bit value indicating how large the rest of the message is in bytes
    msg << (quint32)message.size();
    // currently TCP sockets won't work if the entire message is put into a single byte array
    // this probably has to do with how we are reading them in.
    if ((socketType == TRODESSOCKETTYPE_TCPIP) &&
       (tcpSocket->state() == QAbstractSocket::ConnectedState)) {
        tcpSocket->write(temp); //Write the header to the socket
        if (message.size()) {
            tcpSocket->write(message);
        }
        tcpSocket->flush(); // this is critical if we want the write to happen immediately
    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        // UDP datagrams require that the message be in a single array.
        msg << message;
        udpSocket->writeDatagram(temp, getRemoteHost(), getRemotePort());
        udpSocket->flush();
    }
    else {
        qDebug() << "Socket not ready for writing";
    }
}

void TrodesSocketMessageHandler::sendMessage(TrodesMessage *tm)
{
    sendMessage(tm->messageType, tm->message);
}

// send just the data type without any other data
void TrodesSocketMessageHandler::sendMessage(quint8 dataType)
{
    QByteArray temp(0);
    sendMessage(dataType, temp);
}


QString TrodesSocketMessageHandler::getModuleName()
{
    return _moduleName;
}

bool TrodesSocketMessageHandler::hasName()
{
    return _hasModulename;
}

void TrodesSocketMessageHandler::sendStartAquisition()
{
    sendMessage(TRODESMESSAGE_STARTAQUISITION);
}

void TrodesSocketMessageHandler::sendStopAquisition()
{
    sendMessage(TRODESMESSAGE_STOPAQUISITION);
}

void TrodesSocketMessageHandler::sendTimeRequest()
{
    sendMessage(TRODESMESSAGE_CURRENTTIMEREQUEST);
}

void TrodesSocketMessageHandler::sendTimeRateRequest()
{
    sendMessage(TRODESMESSAGE_TIMERATEREQUEST);
}

void TrodesSocketMessageHandler::sendCurrentStateRequest() {
    sendMessage(TRODESMESSAGE_CURRENTSTATEREQUEST);
}

void TrodesSocketMessageHandler::setDataType(quint8 dType, qint16 userData)
{
    //This is used to set the dataType for a messageHandler that connects to the main trodes server.
    //We set the local datatype and send that information back to trodes.
    dataType = dType;
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << dataType << userData;
//    QMetaObject::invokeMethod(this, "sendMessage",Qt::QueuedConnection, Q_ARG(quint8,TRODESMESSAGE_SETDATATYPE),
//                              Q_ARG(QByteArray, out));
    // qDebug() << "In module " << this->getModuleID() << ". Set data type to " << dType;
    sendMessage(TRODESMESSAGE_SETDATATYPE, out);
}

void TrodesSocketMessageHandler::sendContinuousDataPoint(uint32_t t, int16_t dataPoint)
{
//    QByteArray out;
//    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
//    outStream.setVersion(TrodesDataStream::Qt_4_0);
//    outStream << t << dataPoint;
//    sendMessage(TRODESDATATYPE_CONTINUOUS,out);
    // we need to use a character array to avoid the byte swap that the datastream requires.
    static char data[22];

    struct timespec send_time;

#ifdef __linux__
    clock_gettime(CLOCK_MONOTONIC, &send_time);
#endif

    //printf("TIMESPEC SIZE: %d\n",sizeof(struct timespec));
    memcpy(data, &t, sizeof(uint32_t));
    memcpy(data + 4, &dataPoint, sizeof(int16_t));
    memcpy(data + 4 + sizeof(int16_t), &send_time, sizeof(struct timespec));
    if (t == 0)
        qDebug() << "0 timestamp???";

    sendMessage(TRODESDATATYPE_CONTINUOUS, data, 22); // Threads better align because invokeMethod causes things to break
}

void TrodesSocketMessageHandler::sendSpikeData(uint32_t t, int nPoints, int16_t *waveform)
{
    // Note that this function assumes that the receiver knows the nTrode characteristics

    static char data[MAX_SPIKE_POINTS + sizeof(uint32_t)];

    int waveformSize = nPoints * sizeof(int16_t);

    memcpy(data, &t, sizeof(uint32_t));
    memcpy(data + sizeof(uint32_t), waveform, waveformSize);
    if (t == 0)
        qDebug() << "0 timestamp???";

    sendMessage(TRODESDATATYPE_SPIKES, data, sizeof(uint32_t) + waveformSize); // Threads better align because invokeMethod causes things to break
}

void TrodesSocketMessageHandler::sendDigitalIOData(uint32_t t, int port, char input, char value)
{
    static char data[10], *dataptr;
    // we need to use a structure to avoid the byte swap that the datastream requires.
    /*static DIOBuffer dioBuffer;

    dioBuffer.timestamp = t;
    dioBuffer.port = port;
    dioBuffer.input = input;
    dioBuffer.value = value;
    */
    dataptr = data;
    memcpy(dataptr, &t, sizeof(uint32_t));
    dataptr += sizeof(uint32_t);
    memcpy(dataptr, &port, sizeof(int));
    dataptr += sizeof(int);
    *dataptr = input;
    dataptr++;
    *dataptr = value;

    sendMessage(TRODESDATATYPE_DIGITALIO, data, 10);
}

void TrodesSocketMessageHandler::readData(quint8 *dataType, uint32_t *dataSize, char *data)
{
    if (tcpSocket->read((char*)dataType, sizeof(quint8)) != sizeof(quint8)) {
        qDebug() << "readData: error reading dataType";
        return;
    }
    if (tcpSocket->read((char*)dataSize, sizeof(uint32_t)) != sizeof(uint32_t)) {
        qDebug() << "readData: error reading dataSize";
        return;
    }
    if (tcpSocket->read(data, *dataSize) != *dataSize) {
        qDebug() << "readData: error reading data, size" << dataSize;
        return;
    }
}

void TrodesSocketMessageHandler::sendAnimalPosition(uint32_t t, int16_t x, int16_t y, uint8_t cameraNum)
{
    static char data[9];

    memcpy(data, &t, sizeof(uint32_t));
    memcpy(data + 4, &x, sizeof(int16_t));
    memcpy(data + 6, &y, sizeof(int16_t));
    memcpy(data + 8, &cameraNum, sizeof(uint8_t));
    sendMessage(TRODESDATATYPE_POSITION, data, 9);
}

void TrodesSocketMessageHandler::sendSettleCommand() {
    sendMessage(TRODESMESSAGE_SETTLECOMMAND);
}

void TrodesSocketMessageHandler::sendEvent(uint32_t t, QString eventName)
{

    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t << eventName << QString("");
    sendMessage(TRODESMESSAGE_EVENT, out);
}

void TrodesSocketMessageHandler::sendEvent(uint32_t t, QString eventName, QString moduleName)
{

    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENT, out);
}

void TrodesSocketMessageHandler::sendNewEventNameRequest(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_EVENTNAMEREQUEST, out);
}

void TrodesSocketMessageHandler::sendEventSubscribe(QString eventName, QString moduleName) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENTSUBSCRIBE, out);
}

void TrodesSocketMessageHandler::sendEventUnSubscribe(QString eventName, QString moduleName) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName << moduleName;
    sendMessage(TRODESMESSAGE_EVENTUNSUBSCRIBE, out);
}

void TrodesSocketMessageHandler::sendEventRemoveRequest(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_EVENTREMOVALREQUEST, out);
}

void TrodesSocketMessageHandler::sendEventList(QStringList eventList) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    for (int i=0; i < eventList.length(); i++) {
        outStream << eventList[i];
    }

    sendMessage(TRODESMESSAGE_EVENTLIST, out);
}

void TrodesSocketMessageHandler::requestEventList() {

    sendMessage(TRODESMESSAGE_REQUESTEVENTLIST);
}

void TrodesSocketMessageHandler::sendSubscriptionList(QStringList subsList) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    for (int i=0; i < subsList.length(); i++) {
        outStream << subsList[i];
    }

    sendMessage(TRODESMESSAGE_SUBSCRIPTIONLIST, out);
}

void TrodesSocketMessageHandler::requestSubscriptionList() {
    sendMessage(TRODESMESSAGE_REQUESTSUBSCRIPTIONLIST);
}

/*
void TrodesSocketMessageHandler::sendEventNameRemoved(QString eventName)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_EVENTREMOVED, out);
}

void TrodesSocketMessageHandler::sendNewEventCreated(QString eventName)
{
    //Sent out to all modules to notify the existence of new event
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << eventName;
    sendMessage(TRODESMESSAGE_NEWEVENTCREATED, out);
}*/


quint8 TrodesSocketMessageHandler::getDataType(void)
{
    return dataType;
}

void TrodesSocketMessageHandler::setModuleID(qint8 ID)
{
    moduleID = ID;
}

qint8 TrodesSocketMessageHandler::getModuleID()
{
    return moduleID;
}

void TrodesSocketMessageHandler::setNTrodeId(int trodeId)
{
    nTrodeId = trodeId;
}

void TrodesSocketMessageHandler::setNTrodeIndex(int index)
{
    nTrodeIndex = index;
}

void TrodesSocketMessageHandler::setNTrodeChan(int chan)
{
    nTrodeChan = chan;
}

void TrodesSocketMessageHandler::setDecimation(int dec)
{
    decimation = dec;
}

void TrodesSocketMessageHandler::setSourceSamplingRate(int rate)
{
    sourceSampRate = rate;
}

int TrodesSocketMessageHandler::getNTrodeId(void)
{
    return nTrodeId;
}

int TrodesSocketMessageHandler::getNTrodeIndex(void)
{
    return nTrodeIndex;
}
int TrodesSocketMessageHandler::getNTrodeChan(void)
{
    return nTrodeChan;
}

int TrodesSocketMessageHandler::getDecimation(void)
{
    return decimation;
}


void TrodesSocketMessageHandler::turnOnDataStreaming()
{
    sendMessage((quint8)TRODESMESSAGE_TURNONDATASTREAM);  // the cast ensures the right sendMessage is called
}

void TrodesSocketMessageHandler::turnOffDataStreaming()
{
    sendMessage((quint8)TRODESMESSAGE_TURNOFFDATASTREAM);
}



void TrodesSocketMessageHandler::sendCurrentTime(uint32_t t)
{
    qint64 tm = testTimer.restart();

    if (tm < 5) {
        qDebug() << "Short timestamp send interval: " << tm;
    }

    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << t;
    sendMessage(TRODESMESSAGE_CURRENTTIME, out);
}

void TrodesSocketMessageHandler::sendTimeRate()
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    //outStream << (uint32_t)hardwareConf->sourceSamplingRate;
    outStream << sourceSampRate;
    sendMessage(TRODESMESSAGE_TIMERATE, out);
}

void TrodesSocketMessageHandler::sendOpenFile(QString fileName)
{
    qDebug() << "Sending open file";
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);

    outStream << fileName;

    sendMessage(TRODESMESSAGE_OPENFILE, out);
}

void TrodesSocketMessageHandler::sendSourceConnect(QString connectionType)
{
    qDebug() << "Sending connection type";
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);
    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << connectionType;
    sendMessage(TRODESMESSAGE_SOURCECONNECT, out);
}

//Each module can identify itself with a human-readable name
void TrodesSocketMessageHandler::sendModuleName(QString name)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << name;
    sendMessage(TRODESMESSAGE_MODULENAME, out);
}

void TrodesSocketMessageHandler::sendModuleInstance(int16_t instanceNum) {
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    outStream << instanceNum;
    sendMessage(TRODESMESSAGE_INSTANCENUM, out);
}

void TrodesSocketMessageHandler::sendCloseFile()
{
    sendMessage(TRODESMESSAGE_CLOSEFILE);
}

void TrodesSocketMessageHandler:: sendStartSave()
{
    sendMessage(TRODESMESSAGE_STARTSAVE);
}

void TrodesSocketMessageHandler::sendStopSave()
{
    sendMessage(TRODESMESSAGE_STOPSAVE);
}


//void TrodesSocketMessageHandler::sendConfigRequest() {
//    sendMessage(TRODESMESSAGE_REQUESTCONFIG);
//}

void TrodesSocketMessageHandler::sendCameraImage(const QImage& newImage, int cameraNum)
{
    //Sends an image from the camera. There are up to three camera sources.
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);
    newImage.save(outStream.device(), "JPEG");
    //newImage.save(&out, "JPEG"); // writes image into ba in JPEG format
    //outStream << newImage;

    switch (cameraNum) {
    case 0:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE0, out);
        break;

    case 1:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE1, out);
        break;

    case 2:
        sendMessage(TRODESMESSAGE_CAMERAIMAGE2, out);
        break;
    }
}

void TrodesSocketMessageHandler::sendStateScriptEvent(QString eventString)
{
    QByteArray out;
    TrodesDataStream outStream(&out, QIODevice::WriteOnly);

    outStream.setVersion(TrodesDataStream::Qt_4_0);

    outStream << eventString;
    sendMessage(TRODESMESSAGE_STATESCRIPTEVENT, out);
}

void TrodesSocketMessageHandler::sendQuit()
{
    sendMessage(TRODESMESSAGE_QUIT);
}

void TrodesSocketMessageHandler::closeConnection()
{
    tcpSocket->close();
    emit finished();
}

void TrodesSocketMessageHandler::disconnectHandler()
{
    //tcpSocket->deleteLater();
    emit socketDisconnected();

    if (moduleID != TRODES_ID) {
        // emit a signal to indicate that this was a module that disconnected */
        emit moduleDisconnected(moduleID);
    }
}


void TrodesSocketMessageHandler::errorHandler(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;

    case QAbstractSocket::ConnectionRefusedError:
        emit socketErrorHappened(tr("Connection error: the connection was refused by the peer."));
        break;

    default:
        emit socketErrorHappened(QString("Connection error. %1").arg(tcpSocket->errorString()));
    }
}



//-------------------------------------------------------------------------------------

TrodesClient::TrodesClient(QObject *parent) :
    TrodesSocketMessageHandler(parent)
{
    tcpSocket = new QTcpSocket(this);
    setSocket(tcpSocket);

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
    connect(tcpSocket, SIGNAL(connected()), this, SIGNAL(connected()));

    // connect(tcpSocket,SIGNAL(disconnected()),this,SIGNAL(disconnected()));
}

TrodesClient::TrodesClient(int socketType, QObject *parent) :
    TrodesSocketMessageHandler(parent)
{
    setSocketType(socketType);
    if (socketType == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket = new QTcpSocket(this);
        setSocket(tcpSocket);
        connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
        connect(tcpSocket, SIGNAL(connected()), this, SIGNAL(connected()));
        // connect(tcpSocket,SIGNAL(disconnected()),this,SIGNAL(disconnected()));

    }
    else if (socketType == TRODESSOCKETTYPE_UDP) {
        udpSocket = new QUdpSocket(this);
        connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));

    }
}

void TrodesClient::setAddress(QString addressIn)
{
    address = addressIn;
}

void TrodesClient::setPort(quint16 portIn)
{
    port = portIn;
}

void TrodesClient::connectToHost()
{
    int ntries = 0;
    if (getSocketType() == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket->abort(); //reset the socket in case there is still data there
        while (ntries < 10) {
            tcpSocket->connectToHost(address, port); //connect to host
            // wait for the connection to be established.
            if (tcpSocket->waitForConnected(30000) == false) {
                qDebug("Unable to connect trodes client socket; retrying");
                QObject().thread()->usleep(1000000);
                ntries++;
            }
            else {
                break;
            }
        }
    }
    else if (getSocketType() == TRODESSOCKETTYPE_UDP) {
        // note that for UDP sockets, this just specifies the default connection, so we don't need to wait
        udpSocket->connectToHost(QHostAddress(address), port);
    }
}

void TrodesClient::disconnectFromHost()
{
    if (getSocketType() == TRODESSOCKETTYPE_TCPIP) {
        tcpSocket->disconnectFromHost();
    }
    else if (getSocketType() == TRODESSOCKETTYPE_UDP) {
        udpSocket->disconnectFromHost();
    }
    emit disconnected();
}

QStringList TrodesClient::findLocalTrodesServers()
{
    //We use a 'settings' parameter to tell other modules that a Trodes server is listening
    //Here, we check the settings to get a list of all Trodes servers.  Some of these may not be valid,
    //so we go through and check each one.  If it is not valid, we remove it from the list.  Note that this only works
    //when things are running on a single machine.  For

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("network"));
    QStringList currentTrodesServers = settings.value(QLatin1String("currentServers")).toStringList();
    QStringList checkedTrodesServers;
    //qDebug() << currentTrodesServers;

    for (int i = 0; i < currentTrodesServers.length(); i++) {
        QString currentSelection = currentTrodesServers[i];
        if (!currentSelection.isEmpty()) {
            QStringList currentItems = currentSelection.split(" ");
            if (currentItems.length() >= 2) {
                QString tmpAddress = currentItems[0];
                quint16 tmpPort = currentItems[1].toUInt();

                QTcpSocket* tmpSocket = new QTcpSocket;
                tmpSocket->connectToHost(tmpAddress, tmpPort);
                tmpSocket->waitForConnected(50);
                if (tmpSocket->state() == QAbstractSocket::ConnectedState) {
                    checkedTrodesServers.append(currentSelection);
                }

                tmpSocket->disconnect();
                tmpSocket->deleteLater();
            }
        }
    }
    settings.setValue(QLatin1String("currentServers"), checkedTrodesServers);
    settings.endGroup();

    return checkedTrodesServers;
}



QString TrodesClient::getCurrentAddress()
{
    return address;
}

quint16 TrodesClient::getCurrentPort()
{
    return port;
}



//--------------------------------------------------------------------------

TrodesServer::TrodesServer(QObject *parent) :
    QTcpServer(parent),
    moduleTime(NULL)
{
    address = "";
    stateScriptMessageHandler = NULL;  // initialize this so we can check it later to see if stateScript is running
    sourceTimeRate = 30000;
    // Initialize the module counter
    moduleCounter = 0;
    state_fileOpen = false;
    state_recording = false;
    state_source = "None";
    state_fileName = "";
}

TrodesServer::~TrodesServer()
{
}

void TrodesServer::setModuleTimePtr(const uint32_t *t)
{
    //used to provide a pointer to the module's current clock
    moduleTime = t;
}

void TrodesServer::setModuleTimeRate(int rate)
{
    sourceTimeRate = rate;
}

QString TrodesServer::getCurrentAddress()
{
    return serverAddress().toString();
}

quint16 TrodesServer::getCurrentPort()
{
    return serverPort();
}

QList<QString> TrodesServer::findAvailableAddresses()
{
    QString ipAddress;

    QList<QString> outList;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // find non-localhost IPv4 addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            outList.append(ipAddress);
        }
    }

    return outList;

    // if we did not find one, use IPv4 localhost
    //if (ipAddress.isEmpty())
    //    ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
}

void TrodesServer::setAddress(QString addressIn)
{
    if (!isListening()) {
        address = addressIn;
    }
}

void TrodesServer::startServer(QString identifier, quint16 port)
{
    // set up the code for a new connection
    connect(this, SIGNAL(newConnection()), this, SLOT(newConnectionRequest()));

    //Before we start the server, we need to find which addresses are available
    if (address.isEmpty()) {
        /*
        QList<QString> addressList = findAvailableAddresses();
        if (!addressList.isEmpty()) {
            address = addressList[0];
        }
        else {*/
            //if we did not find a non-localhost address, use localhost
            address = QHostAddress(QHostAddress::LocalHost).toString();
        //}
    }

    QHostAddress currentAddress(address);
    if (!listen(currentAddress, port)) {
        emit socketErrorHappened("Could not use selected address.");
        qDebug() << "Could not start trodes server on host" << address << "port" << port;
        return;
    }
    port = serverPort();

    qDebug() << "Started trodes server on host" << address << "port" << port;


    //We use a 'settings' parameter to tell other modules that a Trodes server is listening
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("network"));
    QStringList currentTrodesServers;
    //QStringList currentTrodesServers = settings.value(QLatin1String("currentServers")).toStringList();
    QString serverLabel;
    serverLabel = QString("%1 %2 %3").arg(address).arg(port).arg(identifier);

    currentTrodesServers.append(serverLabel);
    settings.setValue(QLatin1String("currentServers"), currentTrodesServers);

    settings.endGroup();
}

void TrodesServer::startLocalServer(QString identifier, quint16 port)
{
    connect(this, SIGNAL(newConnection()), this, SLOT(newLocalConnectionRequest()));

    //Before we start the server, we need to find which addresses are available
    if (address.isEmpty()) {

        //QList<QString> addressList = findAvailableAddresses();
        //if (!addressList.isEmpty()) {
        //    address = addressList[0];
        //}
        //else {
            //if we did not find a non-localhost address, use localhost
            address = QHostAddress(QHostAddress::LocalHost).toString();
        //}
    }

    QHostAddress currentAddress(address);
    if (!listen(currentAddress, port)) {
        emit socketErrorHappened("Could not use selected address.");
        qDebug() << "Could not start trodes server on host" << address << "port" << port;
        return;
    }
    port = serverPort();

    qDebug() << "Started local server on host" << address << "port" << port;
}

void TrodesServer::newConnectionRequest()
{
    if (hasPendingConnections()) {
        //A new client is connecting, so create a new socket
        QTcpSocket* socket = nextPendingConnection();

        //connect(socket, SIGNAL(disconnected()),socket, SLOT(deleteLater()));


        messageHandlers.push_back(new TrodesSocketMessageHandler());
        //socket->setParent(messageHandlers.last());
        //messageHandlers.last()->setSocket(socket->socketDescriptor());
        messageHandlers.last()->setSocket(socket);
        messageHandlers.last()->setSocketType(TRODESSOCKETTYPE_TCPIP);
        messageHandlers.last()->setModuleTimePtr(moduleTime); //the moduleTime pointer points to a 32 bit value representing the module's time
        messageHandlers.last()->setSourceSamplingRate(sourceTimeRate);

        connect(messageHandlers.last(), SIGNAL(socketDisconnected()), this, SLOT(clientDisconnectHandler()));
        connect(messageHandlers.last(), SIGNAL(socketErrorHappened(QString)), this, SIGNAL(socketErrorHappened(QString))); // should this be SIGNAL???
        connect(messageHandlers.last(), SIGNAL(setDataTypeReceived(TrodesSocketMessageHandler*, quint8, qint16)), this, SLOT(setDataTypeForConnection(TrodesSocketMessageHandler*, quint8, qint16)));

        connect(messageHandlers.last(), SIGNAL(sendAllDataAvailableReceived(TrodesSocketMessageHandler*)), this, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)));
        connect(messageHandlers.last(), SIGNAL(nameReceived(TrodesSocketMessageHandler*, QString)), this, SLOT(nameReceivedFromModule(TrodesSocketMessageHandler*, QString)));
        connect(messageHandlers.last(), SIGNAL(stateScriptCommandReceived(QString)), this, SLOT(sendStateScript(QString)));
    }
}

void TrodesServer::newLocalConnectionRequest()
{
    if (hasPendingConnections()) {
        //A new client is connecting, so create a new socket
        QTcpSocket* socket = nextPendingConnection();
        //qDebug() << "TrodesServer got local connection request";
        //connect(socket, SIGNAL(disconnected()),socket, SLOT(deleteLater()));


        messageHandlers.push_back(new TrodesSocketMessageHandler());
        //socket->setParent(messageHandlers.last());
        //messageHandlers.last()->setSocket(socket->socketDescriptor());
        messageHandlers.last()->setSocket(socket);
        messageHandlers.last()->setSocketType(TRODESSOCKETTYPE_TCPIP);

        connect(messageHandlers.last(), SIGNAL(setDataTypeReceived(TrodesSocketMessageHandler*, quint8, qint16)), this, SLOT(setDataTypeForConnection(TrodesSocketMessageHandler*, quint8, qint16)));
        connect(messageHandlers.last(), SIGNAL(socketDisconnected()), this, SLOT(clientDisconnectHandler()));
        connect(messageHandlers.last(), SIGNAL(socketErrorHappened(QString)), this, SIGNAL(socketErrorHappened(QString)));
        emit clientConnected();
    }
}

void TrodesServer::sendModuleID(TrodesSocketMessageHandler *messageHandler, qint8 ID)
{
    //qDebug() << "about to send moduleID " << ID;
    // send the moduleID to the module that just connected
    QByteArray msg;
    TrodesDataStream ds(&msg, QIODevice::WriteOnly);

    ds << ID;
    messageHandler->sendMessage(TRODESMESSAGE_MODULEID, msg);
    emit moduleIDSent();
}

void TrodesServer::setNamedModuleMessageHandler(TrodesSocketMessageHandler *messageHandler, QString name)
{
    // set special messageHandler names.  Currently used only for stateScript
    if (name == "stateScript") {
        stateScriptMessageHandler = messageHandler;
    }
}

void TrodesServer::sendFileOpened(QString filename)
{
    //Route to signal
    state_fileOpen = true;
    state_fileName = filename;
    emit signal_send_openfile(filename);
}

void TrodesServer::sendSourceConnect(QString source)
{
    //Route to signal
    state_source = source;
    emit signal_source_connect(source);
}

void TrodesServer::sendFileClose()
{
    state_fileOpen = false;
    emit signal_file_close();
}

void TrodesServer::sendStartRecord()
{
    state_recording = true;
    emit signal_start_record();
}

void TrodesServer::sendStopRecord()
{
    state_recording = false;
    emit signal_stop_record();
}

void TrodesServer::sendCurrentStateToModule(TrodesSocketMessageHandler *messageHandler) {
    if (state_fileOpen) {
        qDebug() << "Trodes: sending file open command to new module";
        messageHandler->sendOpenFile(state_fileName);
        if (state_recording) {
            qDebug() << "Trodes: sending start save command to new module";
            messageHandler->sendStartAquisition();
        }
    }
}

void TrodesServer::relaySettleComand() {
    emit settleCommandTriggered();
}

void TrodesServer::moduleRequstedEventList(qint8 modID) {
    //Sends the event list to the requesting module
    QStringList newList;

    for (int i=0;i < eventNames.length(); i++) {
        //The event name and the module name for that event are interleaved
        newList << eventNames[i];
        newList << eventOwners[i];
    }

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    msg << newList;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENTLIST, m, this);
    sendMessageToModule(modID,tm);
}

void TrodesServer::moduleRequestedSubscriptionList(qint8 modID) {
    //Sends the event subscription list to the requesting module
    QStringList newList;

    for (int i=0;i < eventNames.length(); i++) {
        for (int j = 0; j< eventListenerIDs[i].length(); j++) {
            if (eventListenerIDs[i].at(j) == modID) {
                //The event name and the module name for that event are interleaved
                newList << eventNames[i];
                newList << eventOwners[i];
                break;
            }
        }
    }

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    msg << newList;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_SUBSCRIPTIONLIST, m, this);
    sendMessageToModule(modID,tm);
}

void TrodesServer::sendEventListToModules() {
    //Sends the updated event list to all modules
    QStringList newList;

    for (int i=0;i < eventNames.length(); i++) {
        //The event name and the module name for that event are interleaved
        newList << eventNames[i];
        newList << eventOwners[i];
    }

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    qDebug() << "Sending new event list to modules";
    msg << newList;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENTLIST, m, this);
    sendMessageToModules(tm);
}

void TrodesServer::removeEventTypeFromList(QString eventName, QString moduleName, qint8 modID)
{
    //A module has requested removal of an event
    //If the event exists in the list, remove it
    int index = -1;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleName)==0) ) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        eventNames.removeAt(index);
        eventOwners.removeAt(index);
        eventModIDs.removeAt(index);
        sendEventListToModules();
    }
}

void TrodesServer::addEventTypeToList(QString eventName, QString moduleName, qint8 modID)
{

    //A module has requested
    //If the event name does not already exist, add it to the list
    bool found = false;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleName)==0)) {
            found = true;
        }
    }

    if (!found) {
        eventNames << eventName;
        eventOwners << moduleName;
        eventModIDs.push_back(modID);
        sendEventListToModules();
    }

}

void TrodesServer::eventOccurred(uint32_t t, QString eventName, QString moduleName)
{
    int index = -1;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleName)==0)) {
            index = i;
        }
    }

    if (index > -1) {
        QByteArray m;
        TrodesDataStream msg(&m, QIODevice::ReadWrite);


        msg << t << eventName << eventOwners[index];

        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_EVENT, m, this);

        //Go through the list of listeners for this event, and send out updates
        for (int j=0; j<eventListenerIDs[index].length();j++) {
            sendMessageToModule(eventListenerIDs[index].at(j),tm);
        }
    }
}

void TrodesServer::addListenerToEvent(QString eventName, QString moduleNameOfEvent, qint8 listenerModuleID) {
    int index = -1;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleNameOfEvent)==0)) {
            index = i;
        }
    }

    if (index > -1) {
        bool found = false;
        for (int j=0; j<eventListenerIDs[index].length();j++) {
            if (eventListenerIDs[index].at(j) == listenerModuleID) {
                found = true;
            }
        }
        if (!found) {
            //We didn't find the module ID in the listener list already, so we add it to the list
            eventListenerIDs[index].push_back(listenerModuleID);
        }
    }
}

void TrodesServer::removeListenerFromEvent(QString eventName, QString moduleNameOfEvent, qint8 listenerModuleID) {
    int index = -1;
    for (int i=0;i < eventNames.length(); i++) {
        if ((eventNames[i].compare(eventName) == 0) && (eventOwners[i].compare(moduleNameOfEvent)==0)) {
            index = i;
        }
    }

    if (index > -1) {
        int index2 = -1;
        for (int j=0; j<eventListenerIDs[index].length();j++) {
            if (eventListenerIDs[index].at(j) == listenerModuleID) {
                index2 = j;
                break;
            }
        }
        if (index2 > -1) {
            //Found the module ID to be removed
            eventListenerIDs[index].removeAt(index2);
        }
    }
}

void TrodesServer::addEventTypeToList(QString eventName)
{
    //Called from Trodes
    addEventTypeToList(eventName,"trodes",0);
}

void TrodesServer::removeEventTypeFromList(QString eventName)
{
    //Called from Trodes
    removeEventTypeFromList(eventName,"trodes",0);
}

void TrodesServer::eventOccurred(uint32_t t, QString eventName)
{
    //Called from Trodes
    eventOccurred(t,eventName,"trodes");
}

void TrodesServer::setDataTypeForConnection(TrodesSocketMessageHandler *messageHandler, quint8 dataType, qint16 userData)
{
    //qDebug() << "Server on" << serverAddress() << "port" << serverPort() << "got data type message, dataType = " << dataType;
    messageHandler->setModuleID(TRODES_ID);  // this will be reset below if this a messaging socket for a module
    if (dataType == TRODESDATATYPE_MESSAGING) {
        connect(messageHandler, SIGNAL(currentStateRequested(TrodesSocketMessageHandler*)),this, SLOT(sendCurrentStateToModule(TrodesSocketMessageHandler*)));

        // set up the connection to handle the distribution of the datatypes that this module provides
        connect(messageHandler, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SIGNAL(doAddDataAvailable(DataTypeSpec*)));
        connect(messageHandler, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SLOT(sendDataAvailableToModules(DataTypeSpec*)));

        // set up the connection to send this module the full set of DataAvailable information when it requests it.
        connect(messageHandler, SIGNAL(sendDataAvailableReceived(TrodesSocketMessageHandler*)), this, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)));

        //This socket will be used for all-purpose messaging, so we send the connecting module it's ID and then
        // create a new thread to deal with incoming and outgoing messages.
        // The module counter gets incremented each time there is a new module
        messageHandler->setModuleID(moduleCounter++);
        sendModuleID(messageHandler, messageHandler->getModuleID());

        // Now trigger the trodesNet structure to send the current data available.  Modules that haven't connected yet will trigger another send of their dataAvailable
        //emit tsSendAllDataAvailableToModule(messageHandler);

        // set up a signal to send out a notification to other modules when this module disconnects
        connect(messageHandler, SIGNAL(moduleDisconnected(qint8)), this, SLOT(sendModuleDisconnected(qint8)));

        // connect signals to handler file and other messages from trodes
        connect(this, SIGNAL(signal_send_openfile(QString)), messageHandler, SLOT(sendOpenFile(QString)));
        connect(this, SIGNAL(signal_source_connect(QString)), messageHandler, SLOT(sendSourceConnect(QString)));
        connect(this, SIGNAL(signal_start_record()), messageHandler, SLOT(sendStartAquisition()));
        connect(this, SIGNAL(signal_stop_record()), messageHandler, SLOT(sendStopAquisition()));
        connect(this, SIGNAL(signal_file_close()), messageHandler, SLOT(sendCloseFile()));
        connect(messageHandler, SIGNAL(moduleDataStreamOn(bool)), this, SIGNAL(moduleDataStreamOn(bool)));
        connect(messageHandler,SIGNAL(settleCommandTriggered()),this,SLOT(relaySettleComand()));


        //Event system
        connect(messageHandler, SIGNAL(eventNameAddReqest(QString,QString,qint8)),this,SLOT(addEventTypeToList(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventNameRemoveRequest(QString,QString,qint8)),this,SLOT(removeEventTypeFromList(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventUpdatesSubscribe(QString,QString,qint8)),this,SLOT(addListenerToEvent(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventUpdatesUnSubscribe(QString,QString,qint8)),this,SLOT(removeListenerFromEvent(QString,QString,qint8)));
        connect(messageHandler, SIGNAL(eventListReqested(qint8)),this,SLOT(moduleRequstedEventList(qint8)));
        connect(messageHandler, SIGNAL(eventSubscriptionListRequested(qint8)),this,SLOT(moduleRequestedSubscriptionList(qint8)));
        connect(messageHandler, SIGNAL(eventOccurred(uint32_t,QString,QString)),this,SLOT(eventOccurred(uint32_t,QString,QString)));





        //Otherwise set this to be a dedicated line for data
    }
    else {
        messageHandler->setDedicatedLine(dataType);
        emit newDataHandler(messageHandler, userData);
    }
}


void TrodesServer::nameReceivedFromModule(TrodesSocketMessageHandler *messageHandler, QString moduleName) {

    //Go through all the exisitng module names to see if there is a repeat. If there is, then we need to assign the module an instance number
    QList<int> modCountersFound;

    for (int i=0;i<moduleNames.length();i++) {
        if (moduleNames[i].moduleName == moduleName) {
            //This module is already running
            modCountersFound.append(moduleNames[i].moduleInstance);
        }
    }

    int currentInstance = 1;
    if (modCountersFound.length() > 0) {
        //There is at least one module with this name, so we need to calculate the lowest available instance number
        bool keepGoing = true;
        while (keepGoing) {
            bool modVersionTaken = false;
            for (int i=0;i<modCountersFound.length();i++) {
                if (modCountersFound[i] == currentInstance) {
                    //This number is already taken
                    modVersionTaken = true;
                    currentInstance++;
                    break;
                }
            }
            if (!modVersionTaken) {
                keepGoing = false;
            }
        }
    }

    //Add the module name to the list
    moduleIDNameCombo mnc;
    mnc.moduleID = messageHandler->getModuleID();
    mnc.moduleName = moduleName;
    mnc.moduleInstance = currentInstance;
    moduleNames.append(mnc);

    if (currentInstance > 1) {
        moduleName = moduleName + QString("%1").arg(currentInstance);
    }
    messageHandler->sendModuleInstance(currentInstance);

    qDebug() << "Server: module name " << moduleName << " Instance " << currentInstance;
    emit nameReceived(messageHandler, moduleName);

}

void TrodesServer::deleteServer()
{
    //There should be some code here to close all existing client connections

    close();
    emit finished();
}


void TrodesServer::sendDataAvailableToModules(DataTypeSpec *da)
{
    /* send the new data available information to all the modules */
    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    //qDebug() << "data avail from trodes datatype" << da->dataType << "port" << da->hostPort;
    msg << *da;

    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_DATATYPEAVAILABLE, m, this);
    sendMessageToModules(tm);
}



void TrodesServer::tsSendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler)
{
    // emit a signal that causes the trodesModuleNetwork to send data to the module
    emit doSendAllDataAvailable(messageHandler);
}

void TrodesServer::tsAddDataAvailable(DataTypeSpec *da)
{
    // emit a signal that causes the trodesModuleNetwork to send data to the modules
    emit doAddDataAvailable(da);
}

void TrodesServer::sendStateScript(QString script)
{
    //qDebug() << "in sendStateScript" << (stateScriptMessageHandler == NULL) << "script:" << script;
    if (stateScriptMessageHandler != NULL) {
        QByteArray m;
        m.append(script);

        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_STATESCRIPTCOMMAND, m, this);
        sendMessageToModule(stateScriptMessageHandler, tm);
    }
    else {
        emit trodesServerError("stateScript module not running.\nRestart with correct config file.");
    }
}





void TrodesServer::sendModuleDisconnected(qint8 ID)
{
    // send a message to the modules that another module has disconnected and then emit a signal that removes it's
    // dataAvailable element

    QByteArray m;
    TrodesDataStream msg(&m, QIODevice::ReadWrite);

    msg << ID;
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_MODULEDISCONNECTED, m, this);
    sendMessageToModules(tm);

    emit doRemoveDataAvailable(ID);
}

quint8 TrodesServer::nConnections()
{
    return messageHandlers.length();
}

void TrodesServer::sendMessageToModule(TrodesSocketMessageHandler *messageHandler, TrodesMessage *trodesMessage)
{
    messageHandler->sendMessage(trodesMessage->messageType, trodesMessage->message);
}

void TrodesServer::sendMessageToModule(qint8 modID, TrodesMessage *trodesMessage)
{
    messageHandlers[modID]->sendMessage(trodesMessage->messageType, trodesMessage->message);
}

void TrodesServer::sendMessageToModules(TrodesMessage *trodesMessage)
{
    // go through the list of message handlers and, for each one, send a command to the attached module
    for (int i = 0; i < messageHandlers.length(); i++) {
        if (messageHandlers[i]->getDataType() == TRODESDATATYPE_MESSAGING) {
            messageHandlers[i]->sendMessage(trodesMessage->messageType, trodesMessage->message);
        }
    }
}

void TrodesServer::removeHandlerThread()
{
    //No longer being used, but KEEP just in case we need a way to create new threads
    //for messageHandlers

    QThread *tmpMessageHandlerThread = static_cast<QThread*>(sender());

    tmpMessageHandlerThread->deleteLater();
    messageHandlerThreads.takeAt(messageHandlerThreads.indexOf(tmpMessageHandlerThread));
    qDebug() << "Handler thread removed";
}

void TrodesServer::clientDisconnectHandler()
{
    qDebug() << "Trodes: a module disconnected.";
    TrodesSocketMessageHandler* tmpMessageHandler = static_cast<TrodesSocketMessageHandler*>(sender());



    tmpMessageHandler->closeConnection();
    disconnect(tmpMessageHandler);

    for (int i=0; i < moduleNames.length(); i++) {
        if (moduleNames[i].moduleID == tmpMessageHandler->getModuleID()) {
            moduleNames.takeAt(i);
            break;
        }
    }
    if (state_recording && tmpMessageHandler->getModuleName() == "Camera") {
        qDebug() << "Camera module quit during recording.  Attempting to restart.";
        emit restartCrashedModule(tmpMessageHandler->getModuleName());
    }

    //Memory leak?? deleting causes crash
    messageHandlers.takeAt(messageHandlers.indexOf(tmpMessageHandler));

    //delete messageHandlers.takeAt(messageHandlers.indexOf(tmpMessageHandler));

    //qDebug() << "messageHandler disconnected from" << this->serverAddress().toString() << "port" << serverPort();
    emit clientDisconnected();

    //QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
    //int handlerId = messageHandlers.indexOf()

    //delete buffers.take(socket);
    //delete sizes.take(socket);
    //delete dataTypes.take(socket);

    //socket->deleteLater();
}

TrodesECUClient::TrodesECUClient(QObject *parent) :
    QObject(parent)
{
    udpSocket = new QUdpSocket;
    hardwareAddress.setAddress(networkConf->hardwareAddress);
};

TrodesECUClient::~TrodesECUClient()
{
}

void TrodesECUClient::sendFunctionGoCommand(qint16 functionNum)
{
    qint64 bytesWritten;

    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
#ifdef WIN32
    bytesWritten = udpSocket->writeDatagram((char*)&functionNum, sizeof(functionNum),
                                            QHostAddress::Broadcast, networkConf->hardwarePort);
#else
    bytesWritten = udpSocket->writeDatagram((char*)&functionNum, sizeof(functionNum),
                                            hardwareAddress, networkConf->hardwarePort);
#endif
    if (bytesWritten != sizeof(functionNum)) {
        qDebug() << "Error in writing go command to MCU";
    }
}



TrodesUDPSocket::TrodesUDPSocket(QString hostAddress, QObject *parent) :
    QObject(parent)
{
    // if the hostname is not specified, set it as best we can
    if (hostAddress == "") {
        // first check the network configuration file
        if ((networkConf->networkConfigFound) && (networkConf->trodesHost != "")) {
            hostAddress = networkConf->trodesHost;
        }
        else {
            hostAddress = "127.0.0.1";
        }
    }
    socket = new QUdpSocket(this);
    // bind the socket to the address, port 0, which means that it chooses an open port
    socket->bind(QHostAddress(hostAddress), 0);
    //qDebug() << "UDP socket created on host" << hostAddress << "port" << socket->localPort();
}

TrodesUDPSocket::~TrodesUDPSocket()
{
}

TrodesSocketMessageHandler *TrodesUDPSocket::newDataHandler(int dataType)
{
   // create a new socket and data handler for the socket and set it's data type
    TrodesSocketMessageHandler *newHandler = new TrodesSocketMessageHandler(this);
    newHandler->setDedicatedLine(dataType);
    newHandler->setSocketType(TRODESSOCKETTYPE_UDP);
    // create a udp server for this nTrode
    newHandler->udpSocket = socket;
    connect(newHandler->udpSocket, SIGNAL(readyRead()), newHandler, SLOT(readMessage()));
    return newHandler;
}

QString TrodesUDPSocket::getCurrentAddress() {
    return socket->localAddress().toString();
}

quint16 TrodesUDPSocket::getCurrentPort() {
    return socket->localPort();
}

TrodesModuleNetwork::TrodesModuleNetwork(QObject *parent) :
    QObject(parent)
{
    dataNeeded = 0;
    trodesServerHost = "\0";
    trodesServerPort = 0;
    useQTSocketsForData = true; // set this to false after you create this object if you want to use your own sockets to connect
    dataServerStarted = false;
    moduleName = "\0";
}

TrodesModuleNetwork::~TrodesModuleNetwork()
{
}

void TrodesModuleNetwork::setTcpMessageClientConnected()
{
    trodesClient->setDataType(TRODESDATATYPE_MESSAGING, 0);
}

bool TrodesModuleNetwork::trodesClientConnect()
{
    //No host address given, so we'll need to figure it out.  We also assume this can live in the same thread
    return trodesClientConnect("", 0, false);
}
bool TrodesModuleNetwork::trodesClientConnect(bool sepThread)
{
    //No host address given, so we'll need to figure it out
    return trodesClientConnect("", 0, sepThread);
}

bool TrodesModuleNetwork::trodesClientConnect(QString hostAddress, quint16 hostPort, bool sepThread)

{
    separateThread = sepThread;

    /* create the tcpClient that will connect to trodes */
    trodesClient = new TrodesClient();

    if (hostAddress.isEmpty()) { //no host address given, so we need to figure it out
        qDebug() << "Looking for Trodes server...";
        QStringList availableHosts = trodesClient->findLocalTrodesServers();
        if (availableHosts.length() > 0) {
            //Called whenever something is selected from the drop down menu of addresses
            QString pickedHost = availableHosts[0];

            QStringList currentItems = pickedHost.split(" ");
            if (currentItems.length() >= 2) {
                hostAddress = currentItems[0];
                hostPort = currentItems[1].toUInt();
            }
        }
        else if (networkConf != NULL) {
            // we are likely running on a host that is not running trodes, so we use the information in the config file
            // to set up the socket
            hostAddress = networkConf->trodesHost;
            hostPort = networkConf->trodesPort;
        }
    }

    if (hostAddress.isEmpty()) {
        return false;
    }

    //qDebug() << "trodes host and port: " << hostAddress << " " << hostPort;
    connect(trodesClient, SIGNAL(moduleIDReceived(qint8)), this, SLOT(setModuleID(qint8)));
    connect(trodesClient, SIGNAL(dataAvailableReceived(DataTypeSpec*)), this, SLOT(addDataAvailable(DataTypeSpec*)));
    connect(trodesClient, SIGNAL(moduleDisconnected(qint8)), this, SLOT(removeDataAvailable(qint8)));
    connect(trodesClient, SIGNAL(quitCommandReceived()), this, SIGNAL(quitReceived()));
    connect(trodesClient, SIGNAL(moduleDataChanUpdated(int, int)), this, SIGNAL(moduleDataChanUpdated(int, int)));


    trodesClient->setAddress(hostAddress);
    trodesClient->setPort(hostPort);
    if (separateThread) {
        QThread *trodesNetThread = new QThread;
        //The client will be running in a separate thread.  This code is required to set that up.
        connect(trodesNetThread, SIGNAL(started()), trodesClient, SLOT(connectToHost())); //connect after the thread has started
        connect(trodesClient, SIGNAL(disconnected()), trodesNetThread, SLOT(quit()));
        connect(trodesClient, SIGNAL(disconnected()), trodesClient, SLOT(deleteLater()));
        connect(trodesNetThread, SIGNAL(finished()), trodesNetThread, SLOT(deleteLater()));

        //Function calls need to be routed via signals/slots for multi-threaded behavior
        connect(this, SIGNAL(sig_disconnectClient()), trodesClient, SLOT(disconnectFromHost()));
        connect(this, SIGNAL(sig_SetDataType(quint8, qint16)), trodesClient, SLOT(setDataType(quint8, qint16)));
        connect(this, SIGNAL(sig_sendMessage(quint8)), trodesClient, SLOT(sendMessage(quint8)));
        connect(this, SIGNAL(sig_sendMessage(quint8, const char*, quint32)), trodesClient, SLOT(sendMessage(quint8, const char*, quint32)));
        connect(this, SIGNAL(sig_sendMessage(quint8, int)), trodesClient, SLOT(sendMessage(quint8, int)));
        connect(this, SIGNAL(sig_sendMessage(quint8, QByteArray)), trodesClient, SLOT(sendMessage(quint8, QByteArray)));
        connect(this, SIGNAL(sig_sendMessage(TrodesMessage*)), trodesClient, SLOT(sendMessage(TrodesMessage*)));
        connect(this, SIGNAL(sig_sendTimeRateRequest()), trodesClient, SLOT(sendTimeRateRequest()));
        connect(this, SIGNAL(sig_sendModuleName(QString)), trodesClient, SLOT(sendModuleName(QString)));
        connect(this, SIGNAL(sig_sendCurrentStateRequest()),trodesClient, SLOT(sendCurrentStateRequest()));
        //Start the new thread
        trodesClient->moveToThread(trodesNetThread);
        trodesNetThread->start();
    }
    else {
        trodesClient->connectToHost();
    }

    //Now we wait until the client connects (check at reqular intervals for a few times before giving up)
    bool isConnected = false;
    for (int connectChecks = 0; connectChecks < 5; connectChecks++) {
        qDebug() << "Checking for connection..." << connectChecks + 1;
        if (trodesClient->isConnected()) {
            isConnected = true;
            break;
        }
        QThread::msleep(1000);
    }


    if (!isConnected) {
        qDebug() << "trodesClientConnect failed to connect to trodes server";

        return false;
    }

    if (separateThread) {
        //We now need to tell Trodes that this is a messaging socket so that it sends back our ModuleID
        //We emit a signal instead of a direct call becuase the client now lives in another thread.
        emit sig_SetDataType(TRODESDATATYPE_MESSAGING, 0);
    }
    else {
        trodesClient->setDataType(TRODESDATATYPE_MESSAGING, 0);
    }

    return true;
}

bool TrodesModuleNetwork::trodesECUClientConnect()
{
    ecuClient = new TrodesECUClient(this);
    // start a udp socket to connect to the MCU port that forwards to the ECU a
    if (!ecuClient->udpSocket->bind()) {
        qDebug() << "In trodesECUClientConnect: Error binding to ECU direct port";
        return false;
    }
    qDebug() << "ECU client bound to port" << ecuClient->udpSocket->localPort();
    return true;
}


void TrodesModuleNetwork::disconnectClient()
{
    if (separateThread) {
        emit sig_disconnectClient();
    }
    else {
        trodesClient->disconnectFromHost();
    }
}

void TrodesModuleNetwork::setModuleID(qint8 ID)
{
    moduleID = ID;

    // Now that we've received the module ID, we can check the dataProvided structure to see if we need to start
    // a local data server

    //qDebug() << "Got module ID " << moduleID << ". Now starting server? " << dataProvided.dataTypeSelected();

    // we also send the module name if it is specified
    if (moduleName.length() > 0) {
        qDebug() << "In module " << moduleID << "sending moduleName" << moduleName;
        sendModuleName(moduleName);
    }

    dataServer = NULL;
    dataProvided.moduleID = ID;
    if (networkConf != NULL && networkConf->networkConfigFound) {
        dataProvided.socketType = networkConf->dataSocketType;
    }
    else {
        dataProvided.socketType = TRODESSOCKETTYPE_TCPIP;
    }
    // This ia a bit odd, but we always create a TCP server object
    // This allows for us to put all the messageHandlers in a single list that is part of the TCPIP Server
    //qDebug() << "In module " << moduleID << "dataProvided.dataTypeSelected()" << dataProvided.dataTypeSelected();

    if (dataProvided.dataTypeSelected()) {
        // start a TCPIP server
        dataServer = new TrodesServer(this);
        if (dataProvided.socketType == TRODESSOCKETTYPE_TCPIP) {
            dataServer->startServer("module data server");
            //qDebug() << "In module " << moduleID << "started data server 1";

            dataProvided.hostName = dataServer->getCurrentAddress();
            dataProvided.hostPort = dataServer->serverPort();

        }
        else if (dataProvided.socketType == TRODESSOCKETTYPE_UDP) {
            // start a UDP server, overwriting the host and port info
            //qDebug() << "in setModuleID, localhostname" <<QHostInfo::localHostName();
            //udpDataServer = new TrodesUDPSocket(QHostInfo::localHostName(), this);
            udpDataServer = new TrodesUDPSocket("", this);
            dataProvided.hostName = udpDataServer->getCurrentAddress();
            dataProvided.hostPort = udpDataServer->getCurrentPort();
            // also add a data handler to the dataServer for each type of data provided.
            // Right now, this is only position data from the camera module.  New datatypes will require
            // additional code here to set the UDP ports in the dataProvided structure correctly
            if (dataProvided.dataType & TRODESDATATYPE_POSITION) {
                //qDebug() << "module" << ID << "starting UDP data handler on host" << dataProvided.hostName<< "port" << dataProvided.hostPort;

                TrodesSocketMessageHandler *newHandler = udpDataServer->newDataHandler(TRODESDATATYPE_POSITION);
                dataServer->messageHandlers.push_back(newHandler);
                dataProvided.positionUDPPort = dataProvided.hostPort;
            }
        }
        qDebug() << "module" << ID << "started data server on host" << dataProvided.hostName<< "port" << dataProvided.hostPort;

        QByteArray temp;
        TrodesDataStream msg(&temp, QIODevice::ReadWrite);
        msg << dataProvided;
        if (separateThread) {
            emit sig_sendMessage(TRODESMESSAGE_DATATYPEAVAILABLE, temp);
            emit sig_sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
        else {
            trodesClient->sendMessage(TRODESMESSAGE_DATATYPEAVAILABLE, temp);
            trodesClient->sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE, temp);
        }
        dataServerStarted = true;
        emit dataServerCreated();
    }
    else {
        // get the data type Available information even if we're not providing data, so we ask for it
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        if (separateThread) {
            emit sig_sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
        else {
            trodesClient->sendMessage(TRODESMESSAGE_SENDDATATYPEAVAILABLE);
        }
    }
    // the module ID also defines the local range of statescript functions as follows:
    // module 0  -  fn 10 to 19
    // module 1  -  fn 20 to 29, etc.
    emit stateScriptFunctionRange((ID + 1) * 10, (ID + 1) * 10 + 9);
}

void TrodesModuleNetwork::sendModuleName(QString name)
{
    if (separateThread) {
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        emit sig_sendModuleName(name);
    }
    else {
        trodesClient->sendModuleName(name);
    }
}

void TrodesModuleNetwork::sendCurrentStateRequest()
{
    emit sig_sendCurrentStateRequest();
}

void TrodesModuleNetwork::sendTimeRateRequest()
{
    if (separateThread) {
        //We emit a signal instead of a direct call becuase this function gets called from
        //another thread.
        emit sig_sendTimeRateRequest();
    }
    else {
        trodesClient->sendTimeRateRequest();
    }
}

void TrodesModuleNetwork::sendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler)
{
    // Send the specified module each of the DataAvailable objects

    qDebug() << "Sending all data available structures to module " << messageHandler->getModuleID();
    for (int i = 0; i < dataAvailable.length(); i++) {
        QByteArray m;
        TrodesDataStream msg(&m, QIODevice::ReadWrite);
        msg << dataAvailable[i];
        TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_DATATYPEAVAILABLE, m, this);
        qDebug() << "data avail from module" << dataAvailable[i].moduleID << "datatype" << dataAvailable[i].dataType << "socketType" << dataAvailable[i].socketType << "port" << dataAvailable[i].hostPort;
        emit messageForModule(messageHandler, tm);
    }
}



void TrodesModuleNetwork::addDataAvailable(DataTypeSpec *da)
{
    bool found = false;

    // insert this datatype spec into the main list.
    // If this is from trodes, we just add it to the list, but if it is from a module, we
    // replace the current version if it exists.  This allows modules to come and go */

    if (da->moduleID == TRODES_ID) {
        dataAvailable.push_back(*da);
    }
    else {
        for (int i = 0; i < dataAvailable.length(); i++) {
            if (dataAvailable[i].moduleID == da->moduleID) {
                dataAvailable.replace(i, *da);
                found = true;
            }
        }
        if (!found) {
            dataAvailable.append(*da);
        }
    }
    // Now we check to see if we need a data connection to the module that provided the DataTypeSpec object

    int currentDataType = 1;
    while (currentDataType <= TRODESDATATYPE_MAXDATATYPE) {
        if ((da->dataType & currentDataType) && (dataNeeded & currentDataType)) {
            // this source provides the currentDataType and we need to receive this datatype, so we see if any of the already connected trodesClients provides it.
            found = false;
            for (int i = 0; i < dataClient.length(); i++) {
                if (dataClient[i]->getDataType() == currentDataType) {
                    found = true;
                    qDebug() << "Found client " << i << " data type " << currentDataType;
                    break;
                }
            }
            if (!found) {
                //-------------------
                // Note - This code may not work if TrodesModuleNetwork is started in it's own thread.
                //-------------------

                qint32 dst;
                if (networkConf != NULL) {

                    dst = networkConf->dataSocketType;
                } else {

                   dst = TRODESSOCKETTYPE_TCPIP;
                }

                if (useQTSocketsForData) {
                    if ((currentDataType == TRODESDATATYPE_ANALOGIO) ||
                        (currentDataType == TRODESDATATYPE_DIGITALIO) ||
                        (currentDataType == TRODESDATATYPE_POSITION)) {
                        // create a new client to connect to the specified server.


                        /*
                        TrodesClient *tc = new TrodesClient(networkConf->dataSocketType);
                        //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                        tc->setAddress(da->hostName);
                        if (networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
                            tc->setPort(da->hostPort);
                        }
                        else if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
                            // set the port for the data type
                            if (currentDataType == TRODESDATATYPE_ANALOGIO) {
                                tc->setPort(da->analogIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_DIGITALIO) {
                                tc->setPort(da->digitalIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_POSITION) {
                                tc->setPort(da->positionUDPPort);
                            }
                        }*/


                        TrodesClient *tc = new TrodesClient(dst);
                        //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                        tc->setAddress(da->hostName);
                        if (dst == TRODESSOCKETTYPE_TCPIP) {
                            tc->setPort(da->hostPort);
                        }
                        else if (dst == TRODESSOCKETTYPE_UDP) {
                            // set the port for the data type
                            if (currentDataType == TRODESDATATYPE_ANALOGIO) {
                                tc->setPort(da->analogIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_DIGITALIO) {
                                tc->setPort(da->digitalIOUDPPort);
                            }
                            else if (currentDataType == TRODESDATATYPE_POSITION) {
                                tc->setPort(da->positionUDPPort);
                            }
                        }

                        tc->connectToHost();
                        tc->setDataType(currentDataType, 0);
                        dataClient.append(tc);
                        emit dataClientStarted();
                        qDebug() << "added client for datatype " << currentDataType;
                    }
                    else if (currentDataType == TRODESDATATYPE_CONTINUOUS) {
                        // create one client for data from each nTrode
                        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
                           // check to see if this server provides this nTrode
                           int nTrodeIndex = da->contNTrodeIndexList.indexOf(i);
                           if (nTrodeIndex != -1) {
                                TrodesClient *tc = new TrodesClient(dst);
                                //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                                tc->setAddress(da->hostName);
                                if (dst == TRODESSOCKETTYPE_TCPIP) {
                                    tc->setPort(da->hostPort);
                                }
                                else if (dst == TRODESSOCKETTYPE_UDP) {
                                    // set the port for the data type
                                    tc->setPort(da->contNTrodeUDPPortList.at(nTrodeIndex));

                                }
                                tc->setNTrodeId(spikeConf->ntrodes[i]->nTrodeId);
                                tc->setNTrodeIndex(i);
                                tc->connectToHost();
                                tc->setDataType(currentDataType, tc->getNTrodeIndex());
                                dataClient.append(tc);
                                emit dataClientStarted();
                                qDebug() << "added client for ntrode " << i;
                            }
                        }
                    }
                    else if (currentDataType == TRODESDATATYPE_SPIKES) {
                        // create one client for data from each nTrode
                        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
                           // check to see if this server provides this nTrode
                           int nTrodeIndex = da->spikeNTrodeIndexList.indexOf(i);
                           if (nTrodeIndex != -1) {
                                TrodesClient *tc = new TrodesClient(dst);
                                //qDebug() << "dataAvailable host" << da->hostName << " " << da->hostPort;
                                tc->setAddress(da->hostName);
                                if (dst == TRODESSOCKETTYPE_TCPIP) {
                                    tc->setPort(da->hostPort);
                                }
                                else if (dst == TRODESSOCKETTYPE_UDP) {
                                    // set the port for the data type
                                    tc->setPort(da->spikeNTrodeUDPPortList.at(nTrodeIndex));

                                }
                                tc->setNTrodeId(spikeConf->ntrodes[i]->nTrodeId);
                                tc->setNTrodeIndex(i);
                                tc->connectToHost();
                                tc->setDataType(currentDataType, tc->getNTrodeIndex());
                                dataClient.append(tc);
                                emit dataClientStarted();
                                qDebug() << "added client for ntrode " << i;
                            }
                        }
                    }
                }
                else {
                    // if the module needs to deal with the data clients separately, emit a signal to tell it do so
                    //qDebug() << "module" << moduleName << "in AddDataAvailable, emiting start data client for datatype " << currentDataType;
                    emit startDataClient(da, currentDataType);
                }
            }
        }
        //  move on to the next datatype
        currentDataType *= 2;
    }
}

void TrodesModuleNetwork::removeDataAvailable(qint8 ID)
{
    // remove the entry for this module */
    for (int i = 0; i < dataAvailable.length(); i++) {
        if (dataAvailable[i].moduleID == ID) {
            dataAvailable.removeAt(i);
            break;
        }
    }
}

void TrodesModuleNetwork::sendStateScript(QString *script)
{
    // send the state script on to trodes, which will forward it to the stateScript module
    //qDebug() << "TrodesModuleNetwork: Sending state script to trodes";
    TrodesMessage tm;

    tm.messageType = TRODESMESSAGE_STATESCRIPTCOMMAND;
    tm.message.append(*script);
    if (separateThread) {
        emit sig_sendMessage(&tm);
    }
    else {
        trodesClient->sendMessage(&tm);
    }
}

