/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "customApplication.h"

//This class inherits QApplication, and is used to process
//the command line arguments or OS events such as double clicking
//on a file associated with this program

customApplication::customApplication( int & argc, char **argv )
    : QApplication(argc, argv)
{
    win = new MainWindow();
    if (argc > 1) {
        loadFile(argv[1]);
    }

    win->show();
    win->startThreads();
}
//
customApplication::~customApplication()
{
}
//

void customApplication::loadFile(const QString &fileName)
{
    QFileInfo fi(fileName);
    if (fi.suffix().compare("trodesconf") == 0) {

        win->loadConfig(fileName);
    } else if (fi.suffix().compare("rec") == 0) {

        win->openPlaybackFile(fileName);
    }

}


bool customApplication::event(QEvent *event)
{
        switch (event->type()) {
        case QEvent::FileOpen:
            loadFile(static_cast<QFileOpenEvent *>(
                     event)->file());
            return true;
        default:
            return QApplication::event(event);
    }
}
