/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "audioThread.h"
#include "globalObjects.h"

#include <QtCore/qmath.h>
#include <QtCore/qendian.h>
#include <QDebug>


const int DataFrequencyHz = 44100;
const int BufferSize      = 32768;


//quint64 rawDataWritten = 0; //global variable in globalObjects.h

Generator::Generator(QAudioDeviceInfo dev, const QAudioFormat &format, QObject *parent)
    :   QIODevice(parent)
    ,   listenHWChannel(0)
    ,   listenRefHWChan(0)
    ,   soundDataRead(0)
    ,   totalReadHead(0)
    ,   totalWriteHead(0)
    ,   rawReadIdx(0)
    ,   writeHead(0)
    ,   readHead(0)
    ,   bufferLength(88200*1)
    ,   interpRatio(0.0)
    ,   nextValue(0)
{
    m_buffer.resize(bufferLength);
//    inputFreqHz = (int) hardwareConf->sourceSamplingRate;
    masterGain = 30;
    threshMicroVolts = 15;
    thresh = round((threshMicroVolts*65536)/AD_CONVERSION_FACTOR);
    filterObj.setSamplingRate(hardwareConf->sourceSamplingRate);
}


Generator::~Generator() {

}


void Generator::resetBuffer() {
    writeHead = 0;
    readHead = 0;
    totalReadHead = 0;
    totalWriteHead = 0;
    rawReadIdx = 0;
    soundDataRead = 0;
    interpRatio = 0.0;
    currentValue = 0;
    nextValue = 0;

    for (int i = 0; i < m_buffer.length(); i++) {
        m_buffer[i] = 0;
    }
}

void Generator::start() {
    open(QIODevice::ReadOnly);
}

void Generator::stop() {
    close();
}

qint64 Generator::readData(char *data, qint64 len) {

    //Output written data to soundcard.

    qint64 total = 0;

    if (totalReadHead + len > totalWriteHead) {
        len = totalWriteHead-totalReadHead;
    }

    if (totalReadHead + len <= totalWriteHead) {
        while (len - total > 0) {
            qint64 chunk = 0;
            chunk = qMin((bufferLength - readHead), len - total);
            memcpy(data + total, m_buffer.constData() + readHead, chunk);
            readHead  = (readHead + chunk) % bufferLength;
            totalReadHead += chunk;
            total += chunk;
        }

    } else {
        qDebug() << "End of sound reached";
    }
    return total;
}


qint64 Generator::writeData(const char *data, qint64 len) {

    //Write available data to buffer which will later be read out to sound card.
    qint64 total = 0;
    qint64 chunk = 0;
    while (len - total > 0) {
        chunk = qMin((bufferLength - writeHead), len - total);
        memcpy(m_buffer.data() + writeHead, data + total, chunk);
        writeHead = (writeHead + chunk) % bufferLength;
        totalWriteHead += chunk;
        total += chunk;
    }

    return total;
}

qint64 Generator::bytesWaiting() {
    return totalWriteHead-totalReadHead;
}


qint64 Generator::bytesAvailable() const
{
    return m_buffer.size() + QIODevice::bytesAvailable();
}

int Generator::checkForSamples() {

    //This function takes in the raw data, filters it, interpolates it
    //to 44100Hz, and writes it to another buffer.

    int samplesToCopy = 0;
    int samplesCopied = 0;
    int soundSamplesCopied = 0;
    int inputFreqCorrection = 0;
    samplesToCopy = rawDataWritten - soundDataRead;
    double dataRatio = (double)(inputFreqHz+inputFreqCorrection)/((double) DataFrequencyHz);
    int numConvertSamples = 0;
    //qDebug() << samplesToCopy;

    if (listenHWChannel == -1) {// sound off
      return 0;
    }

    if ((samplesToCopy > 10) && (samplesToCopy < 30000) ) {
        numConvertSamples = (int) round(((double) samplesToCopy)/dataRatio);
        char tempdata[numConvertSamples*2 + 2];
        unsigned char *ptr = reinterpret_cast<unsigned char *>(tempdata);

        while (samplesCopied < (samplesToCopy-1)) {
            currentValue = nextValue;
            if (listenRefHWChan >= 0) {
                //subtract off a reference signal
                 nextValue = static_cast<int16_t>(rawData.data[listenHWChannel + ((rawReadIdx+1) % EEG_BUFFER_SIZE)*hardwareConf->NCHAN] -
                                             rawData.data[listenRefHWChan + ((rawReadIdx+1) % EEG_BUFFER_SIZE)*hardwareConf->NCHAN]);

            }else {
                nextValue = static_cast<int16_t>(rawData.data[listenHWChannel + (((rawReadIdx+1) % EEG_BUFFER_SIZE)*hardwareConf->NCHAN)]);
            }

            //filter the signal
            if (filterOn)
                nextValue = filterObj.addValue(nextValue);


            //implement the digital diode effect

            if (nextValue >= 0) {
                nextValue = qMax(nextValue-thresh, 0);
            } else {
                nextValue = qMin(nextValue+thresh, 0);
            }

            //The signal needs to be interpolated to 44100 Hz
            while (interpRatio < 1) {
                qToLittleEndian<int16_t>(masterGain*(currentValue + interpRatio*(nextValue-currentValue)), ptr);

                ptr += 2;
                soundSamplesCopied++;
                interpRatio += dataRatio;
            }
            interpRatio = interpRatio - floor(interpRatio);
            rawReadIdx = (rawReadIdx + 1) % EEG_BUFFER_SIZE;
            samplesCopied = samplesCopied + 1;

        }

        soundDataRead += (samplesToCopy-1);
        qint64 templen = soundSamplesCopied;
        writeData(tempdata,templen*2);
    }

    return soundSamplesCopied*2;
}

void Generator::setChannel(int hwchannel) {
    listenHWChannel = hwchannel;
    if (hwchannel != -1)
      inputFreqHz = (int) hardwareConf->sourceSamplingRate;

}

void Generator::setRefChannel(int hwchannel) {
    listenRefHWChan = hwchannel;
}

AudioController::AudioController(QObject *parent)
    :   QObject(parent)
    ,   m_device(QAudioDeviceInfo::defaultOutputDevice())
    ,   m_generator(NULL)
{

    initializeAudio();


}


void AudioController::endAudio() {

    //m_pullTimer->stop();
    emit stopTimer();

    //emit stopAudio();
    //emit finished();

}

AudioController::~AudioController() {

    m_audioOutput->stop();
    delete(m_audioOutput);
    delete(m_generator);
}


void AudioController::initializeAudio() {

    qDebug() << "Initializing audio";

    m_buffer.resize(BufferSize);
    hasSupportedAudio = true;

    //m_format.setFrequency(44100);
    m_format.setSampleRate(44100);
    //m_format.setChannels(1);
    m_format.setChannelCount(1);
    m_format.setSampleSize(16);
    m_format.setCodec("audio/pcm");
    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setSampleType(QAudioFormat::SignedInt);


    qDebug() << "Available audio devices:";
    foreach (const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioOutput))
        qDebug() << deviceInfo.deviceName();

    QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());

    if (!info.isFormatSupported(m_format)) {
        qWarning() << "Default format not supported - trying to use nearest";
        m_format = info.nearestFormat(m_format);
        hasSupportedAudio = false;
    }

    createAudioOutput();
}

void AudioController::createAudioOutput() {

    m_audioOutput = new QAudioOutput(m_device, m_format, this);

    m_generator = new Generator(m_device, m_format, this);
    connect(m_audioOutput, SIGNAL(stateChanged(QAudio::State)), SLOT(stateChanged(QAudio::State)));

    m_generator->start();
    m_output = m_audioOutput->start();
}


void AudioController::pullTimerExpired() {

    if (!exportMode && hasSupportedAudio) {
        int bytesTransfered = m_generator->checkForSamples();
        //qint64 bytesWaiting = m_generator->bytesWaiting();

        if (bytesTransfered) {
            //qDebug() << m_audioOutput->periodSize() << bytesTransfered;
            const qint64 len = m_generator->read(m_buffer.data(), bytesTransfered);
            //qDebug() << "Got" << (len) << "Need" << soundBytesDue;
            if (len < 0)
                qDebug() << "Error in QIODevice audiocontroller";

            //m_output->write(m_buffer.data(), bytesTransfered);

            m_output->write(m_buffer.data(), bytesTransfered);
            //qDebug() << m_audioOutput->bytesFree();


        }
    }

}

void AudioController::stateChanged(QAudio::State state)
{
    qDebug() << "state = " << state;
}

void AudioController::setChannel(int hwchannel) {
    m_generator->setChannel(hwchannel);

}

void AudioController::setRefChannel(int hwchannel) {
    m_generator->setRefChannel(hwchannel);
}

void AudioController::updateAudio() {
    //update the reference
    int trodeIndexLookup = streamConf->trodeIndexLookupByHWChan[m_generator->listenHWChannel];
    if (spikeConf->ntrodes[trodeIndexLookup]->refOn) {
        int refTrodeChannel = spikeConf->ntrodes[trodeIndexLookup]->refChan;
        int refTrode = spikeConf->ntrodes[trodeIndexLookup]->refNTrode;
        int refHWChannel = spikeConf->ntrodes[refTrode]->hw_chan[refTrodeChannel];
        setRefChannel(refHWChannel);
    } else {
        setRefChannel(-1);
    }

    //update the filters
    int lowCutoff = spikeConf->ntrodes[trodeIndexLookup]->lowFilter;
    int highCutoff = spikeConf->ntrodes[trodeIndexLookup]->highFilter;
    m_generator->filterObj.setFilterRange(lowCutoff,highCutoff);
    m_generator->filterOn = spikeConf->ntrodes[trodeIndexLookup]->filterOn;
}

void AudioController::setGain(int gain) {
    m_generator->masterGain = gain;
}

void AudioController::setThresh(int thresh) {
    m_generator->threshMicroVolts = thresh;
    m_generator->thresh = round((thresh*65536)/AD_CONVERSION_FACTOR);
}

int AudioController::getThresh() {
    return m_generator->threshMicroVolts;
}

int AudioController::getGain() {
    return m_generator->masterGain;
}

void AudioController::startAudio() {

    m_pullTimer = new QTimer();
    connect(m_pullTimer, SIGNAL(timeout()), SLOT(pullTimerExpired()));
    connect(this,SIGNAL(stopTimer()),m_pullTimer,SLOT(stop()));
    connect(this,SIGNAL(stopTimer()),m_pullTimer,SLOT(deleteLater()));
    m_pullTimer->start(30);

}

void AudioController::stopAudio() {
    m_generator->resetBuffer();
    m_generator->soundDataRead = 0;
    m_generator->rawReadIdx = 0;
}

void AudioController::resetBuffer() {
    m_generator->resetBuffer();
}
